jQuery( function( $ ){	

	jQuery.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
		}
	});
 
 	/*Action : ajax
 	* used to: submit forms
 	* Instance of: Jquery vailidate libaray
	* @JSON 
 	**/

	$("#form").validate({
		errorPlacement: function (error, element) {
			 return;
		},
		highlight: function(element) {
        	$(element).addClass('is-invalid');
        	$(element).parent().addClass("error");
	    },
	    unhighlight: function(element) {
	    	$(element).parent().removeClass("error");
	        $(element).removeClass('is-invalid').addClass('is-valid');
	    },
		submitHandler: function(form){
			// for ( instance in CKEDITOR.instances ){
			// 	CKEDITOR.instances[instance].updateElement();
			// }

			var formData = new FormData($("#form")[0]);
			$.ajax({
			  	beforeSend:function(){
			  		$("#form").find(':submit').attr('disabled',true);
					toastr.clear();
			  	},
			  	url: $("#form").attr('action'),
			  	data: formData,
			  	type: 'POST',
			  	processData: false,
    			contentType: false,
			  	success:function(response){
				  	if(response.success){
				        toastr.success(response.message,'Success');
				        //console.log(response.reload);
				        if (response.reload !='') {
				        	location.reload();
				        }else if (response.redirect_url !='') {
							setTimeout(function(){
								 location.href = response.redirect_url;
							},1000);
						}
				  	}else{
					  
				  	}
			  	},
			  	complete:function(){
			  		$("#form").find(':submit').attr('disabled',false);
					//$("#form").find('button>i').hide(); 
			  	},
              	error:function(status,error){
					var errors = JSON.parse(status.responseText);
					toastr.clear();
					if(status.status == 401){
	                    $("#form").find(':submit').attr('disabled',false);
	                    //$("#form").find('button>i').hide();  
						$.each(errors.error, function(i,v){	
							toastr.error( v[0],'Opps!');
						}); 
					}else{
						toastr.error(errors.message,'Opps!');
					} 				
              	}		  
			});	
			return false;
		}
	});

	$(document).on('submit', '#sendSMSForm', function(event){

		var formData = new FormData($("#sendSMSForm")[0]);
		let file = $('input[type=file]')[0].files[0];
	    if (file !== undefined){ 
	    	formData.append('file', file, file.name);
		}
		
		$.ajax({
		  	beforeSend:function(){
		  		$("#sendSMSForm").find(':submit').attr('disabled',true);
				//$("#sendSMSForm").find('button>i').show(); 
		  	},
		  	url: $("#sendSMSForm").attr('action'),
		  	data: formData,
		  	type: 'POST',
		  	processData: false,
			contentType: false,
		  	success:function(response){
			  	if(response.success){
			  		toastr.clear();
			  		$.each(response.message, function(i,v){	
			  			if(v['success']){
							toastr.success(v['message'],'Success');
			  				$("#sendSMSForm")[0].reset();
			  			}else{
							toastr.error(v['message'],'Error');
						}
					}); 
					if (response.reload !='') {
			        	location.reload();
			        }else if (response.redirect_url !='') {
						setTimeout(function(){
							location.href = response.redirect_url;
						},1000);
					}
			  	}else{
				  	toastr.error(response.message,'Opps!');
			  	}
		  	},
		  	complete:function(){
		  		$("#sendSMSForm").find(':submit').attr('disabled',false);
				//$("#sendSMSForm").find('button>i').hide(); 
		  	},
          	error:function(status,error){
				var errors = JSON.parse(status.responseText);
				toastr.clear();
				if(status.status == 401){
                    $("#sendSMSForm").find(':submit').attr('disabled',false);
                    //$("#sendSMSForm").find('button>i').hide();  
					$.each(errors.error, function(i,v){	
						toastr.error( v[0],'Opps!');
					}); 
				}else{
					toastr.error(errors.message,'Opps!');
				} 				
          	}		  
		});	
		return false;
	});

	$('#showItems').on('change', function() {
		var items = this.value;
		var currentUrl = window.location.href;
		var url = new URL(currentUrl);
		url.searchParams.set("page", 1); 
		url.searchParams.set("item", items); 
		var newUrl = url.href; 
  		window.location.href = newUrl;
	});
	
});

$(document).on('click', '#bulk_delete', function(event){
	event.preventDefault();
	var id = [];
	$('.selectedId:checked').each(function(){
	    id.push($(this).val());
	});
	var href = $(this).data('href');
	//console.log(id);
	if(id.length > 0)
	{
	    $.ajax({
	        url: href,
	        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	        method:"POST",
	        data:{id:id},
	       	success:function(response){
			  	if(response.success){
			        toastr.success(response.message,'Success');
			        if (response.reload !='') {
			        	location.reload();
			        }else if (response.redirect_url !='') {
						setTimeout(function(){
							 location.href = response.redirect_url;
						},3000);
					}
			  	}else{
				  toastr.error(response.message,'error');
			  	}
	  		},
		});
	}
	else{
	    $('#seelectWarning').modal('show');
	}
});

// delete user
$(document).on('click', '#delConfirm', function(event){
	event.preventDefault();
	var href = $(this).attr('href');
    $.ajax({
        url:href ,
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"GET",
       	success:function(response){
		  	if(response.success){
		        toastr.success(response.message,'Success');
		        if (response.reload !='') {
		        	location.reload();
		        }else if (response.redirect_url !='') {
					setTimeout(function(){
						 location.href = response.redirect_url;
					},1000);
				}
		  	}
  		},
	});	
});

// insert variable
$(document).on('submit', '#insertVariableForm', function(){
	
	//var formdata = $(this).serialize();

	if($('#variable_name').val() != ''){
		var variable_name = $('#variable_name').val().replace(" ", "");
	}else{
		var variable_name = $('#variabl_type').val();
	}
	var possible_values = $('#possible_values').val();

	$.ajax({
        url: site_url+"/template/variablecreate/store",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"POST",
        data:{variable_name:variable_name,possible_values:possible_values},
       	success:function(response){

       		$('#variable_name').val('');
			$('#possible_values').val('');
		    $('#variabl_type').html(response.html);
		    $('#insertVariable').modal('hide');
		    variable_name = '##'+variable_name+'##';
			insertAtCursor(document.getElementById('msgCount'), variable_name);
  		},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});
	return false;
});

$(document).on('change', '#variabl_type', function(event){
	event.preventDefault();
	var val = $(this).val();
    $.ajax({
        url: site_url+"/template/get_possible_values",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"GET",
        data:{val:val},
       	success:function(response){		  	
		    $('#possible_values').val(response.data);
  		},
	});	
});

// insert other
$(document).on('submit', '#otherForm', function(){
	
	var formdata = $(this).serialize();
    $.ajax({
        url: site_url+"/template/other/message_type",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"POST",
        data:formdata,
       	success:function(response){
		  	if(response.success){
		        toastr.success(response.message,'Success');
		        $('#messageType').html(response.html);
		        $('#othervaribale').val('');
		        $('#otherpopupmodal').modal('hide');
		  	}
  		},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});	
	return false;
});

$(document).on('submit', '.re_schedule_form', function(){
	
	var formdata = $(this).serialize();
    $.ajax({
        url: site_url+"/reports/re_schedule",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"POST",
        data:formdata,
       	success:function(response){
		  	if(response.success){
		        toastr.success(response.message,'Success');
		        if (response.reload !='') {
		        	location.reload();
		        }else if (response.redirect_url !='') {
					setTimeout(function(){
						 location.href = response.redirect_url;
					},1000);
				}
		  	}
  		},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});	
	return false;
});

function removeTags(str) {
  if ((str===null) || (str===''))
  return false;
  else
  str = str.toString();
  return str.replace( /(<([^>]+)>)/ig, '');
}
function myCustomOnChangeHandler(inst) {
	var text = removeTags(inst.getBody().innerHTML);
  	var count = text.length;
  	$('#charecterShow').text(count);
  	$('#msgCount').val(text);
}

tinymce.init({
selector: '#msgCount',
plugins: "autoresize link",
toolbar_location: 'bottom',
menubar:false,
statusbar: false,
toolbar: "link",
target_list:false,
link_title:false,
setup: function (ed) {
        ed.on("keyUp", function () {
            myCustomOnChangeHandler(ed);
        })
        ed.on('init',function(e) {
            tinyMceEditLink(ed);
        });
    }
});

function tinyMceEditLink(editor) {

    $('.tox-toolbar__group button span').append('Link');	
    editor.windowManager.oldOpen = editor.windowManager.open;  // save for later
    editor.windowManager.open = function (t, r) {    // replace with our own function
        var modal = this.oldOpen.apply(this, [t, r]);  // call original
        $('.tox-dialog__footer-end button:nth-child(2)').text('Insert');
        
        /*if (t.title === "Insert/Edit Link") {
            $('.tox-dialog__footer-end').append(
                '<button title="Custom button" type="button" data-alloy-tabstop="true" tabindex="-1" class="tox-button" id="custom_button">Custom button</button>'
            );

            $('#custom_button').on('click', function () {
                //Replace this with your custom function
                console.log('Running custom function')
            });
        }*/

        return modal; // Template plugin is dependent on this return value
    };
}

function insertAtCursor(myField, myValue) {

  	tinymce.init({
		selector: '#msgCount'
	});
    //IE support
    if (document.selection) {
        myField.focus();
        sel = document.selection.createRange();
        sel.text = myValue;
    }
    // Microsoft Edge
    else if(window.navigator.userAgent.indexOf("Edge") > -1) {
      var startPos = myField.selectionStart; 
      var endPos = myField.selectionEnd; 
          
      myField.value = myField.value.substring(0, startPos)+ myValue 
             + myField.value.substring(endPos, myField.value.length); 
      
      var pos = startPos + myValue.length;
      myField.focus();
      myField.setSelectionRange(pos, pos);
    }
    //MOZILLA and others
    else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)
            + myValue
            + myField.value.substring(endPos, myField.value.length);
            //alert(myField.value);
    } else {
        myField.value += myValue;
    }
    tinymce.execCommand('mceInsertContent', false, myValue);
    //var count = myValue.length;
    var new_length = myValue.length;
    var old_length = parseInt($('#charecterShow').text());
  	$('#charecterShow').text(new_length+old_length);
    //tinymce.activeEditor.setContent(myField.value);
}

$(function() {
    $('#allAppAccessCheck').click(function() {
      if ($(this).prop('checked')) {
          $('.app_access_check').prop('checked', true);
      } else {
          $('.app_access_check').prop('checked', false);
      }
    });
    $(".app_access_check").click(function(){
      var numberOfCheckboxes = $(".app_access_check").length;
      var numberOfCheckboxesChecked = $('.app_access_check:checked').length;
      if(numberOfCheckboxes == numberOfCheckboxesChecked) {
         $("#allAppAccessCheck").prop("checked", true);
      } else {
         $("#allAppAccessCheck").prop("checked", false);
      }
    });
});

$(document).on('change', '#template_id', function(){
	
	var template_id = $(this).val();
	let formData = new FormData($('#sendSMSForm')[0]);
    let file = $('input[type=file]')[0].files[0];
    //console.log(file);
    if (file !== undefined){ 
    	formData.append('file', file, file.name);
	}
    $.ajax({
        url: site_url+"/template/get_template_data",
        method:"POST",
        contentType: false,
        processData: false,   
        cache: false, 
        data:formData,
       	success:function(response){
		  	if(response.success){
		  		$('#dynamic_value_here').html(response.dynamic_value_html);
		  		if(response.dynamic_value_html !=''){
		  			$('#send_sms_without_csv').hide();
		  			$('#send_sms_with_csv').show();
		  		}
		        $('#message').val(response.data);
		  	}
  		},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});	
	return false;
});

$(document).on('click', '.send_sms_with_csv_btn', function(){
	
	let formData = new FormData($('#sendSMSForm')[0]);
    let file = $('input[type=file]')[0].files[0];
    if (file !== undefined){ 
    	formData.append('file', file, file.name);
	}
	var that = $(this);
	var send_type = that.data('type');
	formData.append('send_type',send_type);

    $.ajax({
        url: site_url+"/sendsms/check_variables_mapping",
        method:"POST",
        processData: false,
	    contentType: false,
	    cache: false,
        data:formData,
        beforeSend:function(){
	  		that.attr('disabled',true);
			that.find('i').show(); 
	  	},
       	success:function(response){

		  	if(response.success){

		  		$('#missing_template_popup').modal('hide');
		  		$('#send_sms_data').html(response.data);
		  		$('#send_sms').modal('show');
		  		$('#log_text').val(response.error);
		  	}else{
		  		$('#missing_template_variables').html(response.data);
		  		$('#missing_template_popup').modal('show');
		  		$('#log_text').val(response.error);
		  	}
  		},
  		complete:function(){
	  		that.attr('disabled',false);
			that.find('i').hide(); 
	  	},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});	
});

$('#uploadCSV').change(function() {
    
    let file = $('input[type=file]')[0].files[0];
    var ext = (file.name).split('.').pop().toLowerCase();
    if(ext == 'csv'){
    	$('.attachFileSpan').html('<i class="fa fa-refresh" aria-hidden="true"></i> Uploaded');
    	$('.attachFileSpan').css({"color":"#fff", "background":"#5FCBEA", "font-weight":"bold"});
    }else{
    	$('.attachFileSpan').html('<i class="fa fa-upload" aria-hidden="true"></i> Upload your CSV');
    	$('.attachFileSpan').removeAttr('style');
    	toastr.error('Invalid file, only allow is csv file','Error');
    }
});

$('#uploadCSV_no_need').change(function() {
    let formData = new FormData($('#sendSMSForm')[0]);
    let file = $('input[type=file]')[0].files[0];
    formData.append('file', file, file.name);
    $.ajax({
        url: site_url+"/template/ajaxUploadCSV",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'POST',   
        contentType: false,
        processData: false,   
        cache: false,        
        data: formData,
        success: function(response) {
            $('#phone_number').val(response.data);
            toastr.success(response.message,'Success');
        },
        error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Error!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
    });
});

// insert other
$(document).on('click', '.api_preview_btn', function(){
	
	var template_id = $(this).data('id');
	$('#previewTemplateModal').modal('show');
    $.ajax({
        url: site_url+"/getTemplatePreview",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        method:"POST",
        data:{template_id:template_id},
       	success:function(response){
		  	if(response.success){
		        $('#api_preview').html(response.html);
		  	}
  		},
  		error:function(status,error){
			var errors = JSON.parse(status.responseText);
			if(status.status == 401){ 
				$.each(errors.error, function(i,v){	
					toastr.error( v[0],'Opps!');
				}); 
			}else{
				toastr.error(errors.message,'Opps!');
			} 				
      	}
	});	
	return false;
});