function updateCount() {
    var count = $(this).val().length;
    $('#charecterShow').text(count);
}

//show character count on edit.
$(document).ready(function () {

  setTimeout(function() { $(".alert").hide(); }, 2000);

  $('.datepickeronly').datepicker({
      dateFormat: 'yy-mm-dd',
  });
  
  $('.datetimepicker').datetimepicker({
      minDate: new Date(),
      format: 'yy-m-d H:i',
      useCurrent: false,
      showClose: true,
      showClear: true
  });

  $("#exportExcelReports").click(function(){
    $('#exp_from_date').val($('#from_date').val());
    $('#exp_to_date').val($('#to_date').val());
    $('#exp_app_id').val($('#app_id').val());
    $('#exp_message_source').val($('#message_source').val());
    $('#export_for_reports').submit();
  });

  $('#selectall').click(function () {
      $('.selectedId').prop('checked', this.checked);
  });

  $('.selectedId').change(function () {
      var check = ($('.selectedId').filter(":checked").length == $('.selectedId').length);
      $('#selectall').prop("checked", check);
  });

  $(document).on('change','#messageType', function() {
      if($(this).val() == 'Other Message Type'){
        $('#otherpopupmodal').modal('show');
      }
  });
  
  $('#otherpopupmodal').on('hidden.bs.modal', function () {
      $('#messageType')[0].selectedIndex = 0; 
  });

  $('.delthis').on('click', function() {
    $('.selectedId').removeAttr('checked');
    $(this).parents('tr').find('th input').attr('checked',true);
  }); 
  $('.insertVariable').on('click', function() {
    //$('#insertVariable').
  });
  $("#deleteTemplate, #deleteUsers").on('hide.bs.modal', function(){
    $('.selectedId').removeAttr('checked');
  });

  $('.delete').on('click', function() {
    var href_url = $(this).data('href');
    $('#delConfirm').attr('href',href_url);
  }); 
  
  $('#bulk_delete_btn_template').on('click', function() {
      var ids = [];
      $('.selectedId:checked').each(function(){
          ids.push($(this).val());
      });
      if (Array.isArray(ids) && ids.length){
        $('#seelectWarning').modal('hide');
        $('#deleteTemplate').modal('show'); 
      }else{
        $('#deleteTemplate').modal('hide');
        $('#seelectWarning').modal('show');
      }
  });

  $('#bulk_delete_btn_user').on('click', function() {
      var ids = [];
      $('.selectedId:checked').each(function(){
          ids.push($(this).val());
      });
      if (Array.isArray(ids) && ids.length){
        $('#seelectWarning').modal('hide');
        $('#deleteUsers').modal('show'); 
      }else{
        $('#deleteUsers').modal('hide');
        $('#seelectWarning').modal('show');
      }
  });

  $(document).on('change','.csv_headers', function() {
      if($(this).val() == 'custom_value'){
        $(this).parents('.form-space').find('.custom_val').show();
      }else{
        $(this).parents('.form-space').find('.custom_val').hide();
      }
  });

  $(document).on('click','.send_type', function() {
    var send_type = $(this).data('type');
    $('#send_type').val(send_type);
  });

 //validation
  $('.phone').inputmask({
      mask: "+1 999 999 9999",
      greedy: true,
      placeholder: " "
  });

  $.validator.addMethod("phoneno", function (phone_number, element) {
      phone_number = phone_number.replace(/\s+/g, "");
      return this.optional(element) || phone_number.length > 9 &&
      phone_number.match(/^\+[0-9]{10}$/);
  }, "Please specify a valid phone number");

  $.validator.addMethod("laxEmail", function(value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional( element ) || /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test( value );
  }, 'Please enter a valid email address.');

  $(".phone, .fax").each(function (item) {
      $(this).rules("add", {
        phoneUS: true,
        required: true
      });
  });

  $(".email").each(function (item) {
    $(this).rules("add", {
      email: true,
      laxEmail:true
    });
  });
  
  $.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0; 
  }, "No space please and don't leave it empty");

  /*$("form[id='insertVariableForm']").validate({
    rules: {
      variable_name: {
        noSpace: true
      }
    },
    messages: {
      variable_name: {
        noSpace: "White space not allowed"
      }
    }
  });
  */

});

/*Show maximum 5 pages*/
(function($) {
    $('ul.pagination li.active')
        .prev().addClass('show-mobile')
        .prev().addClass('show-mobile')
        .prev().addClass('show-mobile');
    $('ul.pagination li.active')
        .next().addClass('show-mobile')
        .next().addClass('show-mobile')
        .next().addClass('show-mobile');
    $('ul.pagination')
        .find('li:first-child, li:last-child, li.active')
        .addClass('show-mobile');
})(jQuery);