<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendSms extends Model
{
    protected $fillable = [
        'phone_numbers', 'template_id', 'message', 'send_type','schedule_time','response','created_by','sid'
    ];
}
