<?php 
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
/**
 * Method to generateCode.
 *
 * @param  $codePrefix, $objectId, $provenceId
 * @return generatedCode
 */

/**
* Method to generate unique code
* @param $db is object of databse
* @param $table is name of table
* @site_id is like PARTS.DENTAL, PAYMENT.DENTAL
* @key is like order_no, invoice_no, dn_no etc
* @prefix is like SO, IN, QT, DN etc
*/
function master_unique_code(Illuminate\Support\Facades\DB $db, $table, $prefix,$key){
 
  $results = $db::table($table)->get();
  if(!empty($results)){
    $codeArr = array();
    foreach($results as $row){      
      $codeArr[] = str_ireplace($prefix,'',$row->$key);
    }
    $sr_no = (count($codeArr)>0)?(max($codeArr)+1):1;
    
    $numbers= str_pad($sr_no, 3, '0', STR_PAD_LEFT);
    $generatedCode = sprintf("%s%s",$prefix, $numbers);
  }else{
    $numbers= str_pad(1, 3, '0', STR_PAD_LEFT);
    $generatedCode = sprintf("%s%s", $prefix, $numbers);
  }
  return  $generatedCode; 
}

function get_pagination(){

  $pagination = [
                  5 => 'Show up to 5 items',
                  10 => '10 items',
                  15 => '15 items',
                  25 => '25 items',
                  50 => '50 items'
                ];
    return $pagination;
}
function sendMessage($recipient, $message)
{
  /*$TWILIO_SID  = 'AC3cf062aee19ea53090620ce77a1167a2';
  $TWILIO_AUTH_TOKEN  = 'e474db17a3995823f9664508971dc31b';
  $TWILIO_NUMBER  = '+17739853176';
  */
  $TWILIO_SID  = 'AC2f58917d5af73f7fbdb4856cbda3244b';
  $TWILIO_AUTH_TOKEN  = '2edaad433f6b376a45de1f3218d4e397';
  $TWILIO_NUMBER  = '+17328632233';
  
  if(strpos('+91',$recipient) === false){
    //$recipient = '+91'.$recipient;
  }
  $message = str_replace('&nbsp;', '', $message);
  try{
      $client = new Client($TWILIO_SID, $TWILIO_AUTH_TOKEN);
      $response = $client->messages->create($recipient, ['from' => $TWILIO_NUMBER, 'body' => $message]); 
      return json_encode([
              'success'   => true,
              'message'   => 'Message has been sent to '.$recipient,
              'sid'       => $response->sid
          ]);
  }catch(RestException $e){
      return json_encode([
              'success'   => false,
              'message'   => $e->getMessage(),
              'sid'       => ''
          ]);
  }        
}
?>