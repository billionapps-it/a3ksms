<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $fillable = [
        'title', 'message_type', 'choose_app', 'message'
    ];
    public function appType(){
        return $this->hasOne('App\AppType','id','choose_app');
    }
}
