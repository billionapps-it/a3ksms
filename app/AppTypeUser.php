<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppTypeUser extends Model
{
    protected $table = 'app_type_user';

}