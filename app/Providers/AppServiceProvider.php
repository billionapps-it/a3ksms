<?php
namespace App\Providers;

use View;
use Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
	public function boot()
    {
        
        \Response::macro('attachment', function ($content) {

		    $headers = [
		        'Content-type'        => 'text/html',
		        'Content-Disposition' => 'attachment; filename="error_log.txt"',
		    ];

	    	return \Response::make($content, 200, $headers);
		});
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
