<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppType extends Model
{
    protected $table = 'app_type';
    protected $fillable = [
        'name', 'auth_key'
    ];
    
}