<?php

namespace App\Exports;

use App\SendSms;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class DeliveryReportsExport implements FromView
{
	public $from_date = '';
	public $to_date   = '';
	public $app_id    = '';
	public $message_source = '';

	public function __construct($data)
    {
    	$this->from_date 		= $data['exp_from_date'];
	    $this->to_date 			= $data['exp_to_date'];
	    $this->app_id 			= $data['exp_app_id'];
	    $this->message_source  	= $data['exp_message_source'];
    }
    public function view(): View
	{
        $collection = SendSms::select('send_sms.*');     

        if(isset($this->app_id) && $this->app_id > 0){
			$collection->where('send_sms.app_id', $this->app_id);
        }

        if(isset($this->message_source)){

        	if($this->message_source == 'Web'){
				$collection->where('send_sms.app_id', NULL);
			}else if($this->message_source == 'API'){
				$collection->where('send_sms.app_id', '>=', 0);
			}
        }

        if(isset($request->from_date) && isset($request->to_date)){
            $from_date = $request->from_date;
            $to_date   = $request->to_date;
            $collection->whereBetween('send_sms.created_at', [$from_date.' 00:00:00', $to_date.' 23:59:59']);
        }elseif(isset($request->from_date)){
            $from_date = $request->from_date;
            $collection->whereDate('send_sms.created_at', $from_date);
        }elseif(isset($request->to_date)){
            $to_date   = $request->to_date;
            $collection->whereDate('send_sms.created_at', $to_date);
        }

        $collection->orderBy('send_sms.created_at','desc');
        $data = $collection->get();
        return view('admin.reports.exports', compact('data'));
    }
}
