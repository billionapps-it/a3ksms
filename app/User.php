<?php
namespace App;

use Kodeine\Metable\Metable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Metable;

	protected $metaTable = 'users_meta'; 

	public $hideMeta = false; 

	use HasRoles;
	
    use Notifiable, HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','status','is_email_verified','providerId','updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    //Delete the data based on user
    public static function boot() {
        parent::boot();

        static::deleting(function($user) {
             $user->metas()->delete();
        });
    }

    public function delete_role(){
        return $this->hasOne(UserRole::class);
    }

    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    public function appAccesses()
    {
      return $this->belongsToMany('App\AppType');
    }
   
    
    /**
    * @param string|array $roles
    */
    public function authorizeRoles($roles)
    {
      if (is_array($roles)) {
          return $this->hasAnyRole($roles) || 
          abort(401, 'This action is unauthorized.');
      }
      return $this->hasRole($roles) || 
          abort(401, 'This action is unauthorized.');
    }
    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
      return null !== $this->roles()->whereIn('name', $roles)->first();
    }
    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
      return null !== $this->roles()->where('name', $role)->first();
    }
  
    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->hideMeta ?
            parent::toArray() :
            array_merge(parent::toArray(), [
                'meta_data' => $this->custom_meta_data(),
            ]);
    }
}
