<?php
namespace App\Http\Controllers\API;
 
use File; 
use Session;
use Validator;
use App\Role;
use App\User;
use App\Template;
use App\SendSms;
use App\AppType;
use Socialite;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request; 
use App\Mail\RetrivePassword;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;

class APIController extends Controller
{
    public function sendSMS(Request $request)
    {
        //dd($request->all());
        $template_id = $request->template_id;
        $app_id      = $request->app_id;
        //$message     = $request->message;
        $recipient_number = $request->recipient_number;//( as array) 
        $dynamic_value = $request->dynamic_value;//( as array) 
        $schedule_datetime = $request->schedule_datetime;
        //dd($dynamic_value);
        //Start Validation
        $messages = [
          'template_id.required' => 'template_id field is required.',
          'recipient_number.required'   => 'recipient_number field is required.',
        ];

        $validator = Validator::make($request->all(), [
            'template_id' => 'required',
            'recipient_number' => 'required',
        ],$messages);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);            
        } 
        //end Validation

        $msg     = array();
        //$recipients = explode(',', $recipient_number);
        /*$token = array(
            'ACCEPT_LINK'   => $smsArr['accept_link'],
            'VIDEO_LINK'    => $smsArr['video_link'],
            'NAME'          => $smsArr['invite_name'],
            'PRACTICE_NAME' => $smsArr['practice_name'],
            'DOCTOR_NAME'   => $smsArr['doctor_name'],
            'FRIEND_NAME'   => $smsArr['friend_name']
        );*/
        $app = AppType::where('id',$app_id)->first();
        if(!$app){
            return response()->json([
                'success'       => false,
                'message'       => 'Invalid app_id'
            ],401);
        }

        $template = Template::where('template_id',$template_id)->first();
        if(!$template){
            return response()->json([
                'success'       => false,
                'message'       => 'Invalid template_id'
            ],401);
        }
        $message = $template->message;

        $pattern = '##%s##';
        if(!empty($dynamic_value)){
            foreach($dynamic_value as $values){
                //dd($val);
                foreach($values as $key=>$val){
                    $varMap[sprintf($pattern,$key)] = $val;
                }
            }
            $new_message = strtr($message,$varMap);
        }else{
            $new_message = $message;
        }
        //echo $new_message;exit;
        if(!empty($recipient_number)){

            if($schedule_datetime !=''){
                $send_type = 'scheduled';
                $schedule_time = $schedule_datetime;
            }else{
                $send_type = 'one_time';
                $schedule_time = NULL;
            }

            foreach ($recipient_number as $recipient) {

                if($send_type == 'one_time'){

                    $response = sendMessage($recipient, $new_message);
                    $response_arr = json_decode($response,true);
                    $msg[] = $response_arr;
                    $is_sent = (!empty($response_arr) && ($response_arr['sid']>0))?1:0;

                }else{

                    //scheduled
                    $is_sent = 0;
                    $response_arr = array();
                    $msg[] = array('success'=>true,'message'=>'Your message has been scheduled successfully.');
                }

                //save to db
                $sendsms = new SendSms();
                $sendsms->phone_numbers = $recipient;
                $sendsms->template_id   = $template_id;
                $sendsms->message       = $new_message;
                $sendsms->send_type     = $send_type;
                $sendsms->schedule_time = $schedule_time;
                $sendsms->is_sent       = $is_sent;
                $sendsms->created_by    = 1;
                $sendsms->sid           = !empty($response_arr)?$response_arr['sid']:'';
                $sendsms->app_id        = $app_id;
                $sendsms->send_source   = 'API';
                $sendsms->response      = !empty($response_arr)?$response_arr['message']:'';                
                $sendsms->save();
            }    
            return response()->json([
                'success'       => true,
                'message'       => $msg
            ]);

        }else{
            return response()->json([
                'success'       => false,
                'message'       => 'Wrong recipients format.'
            ],401);
        }
    }
}