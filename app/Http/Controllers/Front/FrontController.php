<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Crypt;
use Hash;
use App\User;

class FrontController extends Controller
{
    public function resetpassword(Request $request, $token)
    {
        $id = Crypt::decrypt($token);
        $user = User::find($id)->first();
        if(!$user){
            return redirect()->back()->with('error','Either token is invalid or expired!!');
        }
        if($request->isMethod('post') && $request->ajax()){

            $messages = [
              'password.required' => 'Password field is required.',
              'password_confirmation.required'   => 'Confirm password field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'password_confirmation' => 'required|min:6',
                'password'   => 'required|min:6|required_with:password_confirmation|same:password_confirmation',
            ],$messages);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            $user->password = Hash::make($request->newpassword);
            $user->save();
            
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'message'   =>'Your password has been updated successfully, Thank you.',
                    'reload'=>'',
                    'redirect_url' => url('login')
            ]);
        }
        return view('front.resetPassword',compact('token'));
    }
}