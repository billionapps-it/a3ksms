<?php
namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        \Validator::extend('without_spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required|without_spaces',
        ],[
            'email.required'=> 'Please enter email',
            'password.required'=> 'Please enter password',
            'password.without_spaces'=> 'White space not allow in password',
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()
                   ->withInput()
                   ->withErrors($validator);
        }

        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        $remember_me = ( !empty( $request->remember_me ) )? TRUE : FALSE;

        if(Auth::attempt($credential)){
            $user = User::where(["email" => $credential['email']])->first();
            Auth::login($user, $remember_me);
            return redirect('templates');
        }else{
            return redirect()->back()->withInput()
                    ->with('error','Invalid credentials');
        }
    }
    public function logOut() {

        //activity log
        //logActivity('logOut', 0, 'App\User');

        Auth::logout();
        return redirect('login');
    }

    public function redirectTo()
    { 
        //activity log
        //logActivity('Logged in', 0, 'App\User');
        switch(Auth::user()->roles()->get()->first()->name){
            case 'admin':
				$this->redirectTo = RouteServiceProvider::ADMIN_HOME;
				break;
			case 'super_admin':
				$this->redirectTo = RouteServiceProvider::SUPER_ADMIN_HOME;
				break;
			
        }
		return $this->redirectTo;
    }
}
