<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use App\User;
use App\Role;
use App\AppType;
use App\UserRole;
use App\AppTypeUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Crypt;

class UserController extends Controller
{
	public const REDIRECT_URL = '/users';

    public function index(Request $request){

        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;

        $users = User::whereHas('roles', function($q){
                    $q->where('name', 'admin');
                })->paginate($current_page);
        
        $users->appends($request->all());
        if(isset($request->page)){
            $sr_no_start = ($request->page * $current_page)-$current_page + 1;
        }else{
            $sr_no_start = 1;
        }    	
        return view('admin.users.index',compact('users','current_page','sr_no_start'));
    }
    public function store(Request $request)
    {
        //Create User
        if( $request->isMethod('post') && $request->ajax()){
            //Start Validation
            Validator::extend('without_spaces', function($attr, $value){
                return preg_match('/^\S*$/u', $value);
            });

            $messages = [
              'first_name.required' => 'Username field is required.',
              'email.required' => 'Email field is required.',
              'phone.required' => 'Phone field is required.',
              'password.required' => 'Password field is required.',
              'password.without_spaces' => 'Empty space not allow in password',
            ];
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
                'password' => 'required|without_spaces|min:6|max:12',
                'email' => 'required|email|unique:users',
                'first_name' => 'required|max:26',
            ],$messages);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            $user = new User();
            foreach($request->all() as $key=>$value){
                if( in_array( $key,$user->getFillable() ) ){
                    $user->$key = $value;
                }
            }   
            $user->password = Hash::make ( $request->get ('password') );      
            $user->save();

            $user_id = $user->id;

            $user->roles()->attach(Role::where('name', $request->role_name)->first());
            $user->save();

            $user->appAccesses()->attach($request->app_access);

            $token = Crypt::encryptString($user_id);
            $link  = url('resetpassword/'.$token);
            $data['link'] = $link;
            $data['first_name'] = $user->first_name;
            $data['last_name'] = $user->last_name;

            try{
                Mail::send('emails.forgotpassword', $data, function($message) use($user)
                {
                    $message->to($user->email, 'Reset Password')->subject('Reset Password');
                    $message->from('qa@billionapps.net', 'Achieve3000');
                });
                if (!Mail::failures()) {
                    return response()->json([
                            'success' => true,
                            'data'   => [],
                            'reload'  => '',
                            'message'   =>'User has been created & reset password link sent.',
                            'redirect_url'  => url('users')
                    ]);
                }else{
                    return response()->json([
                        'success' => false,
                        'message' =>'User has created but email sending having some issues'
                    ],401);    
                }
            }catch(\Swift_TransportException $e){
                return response()->json([
                    'success' => false,
                    'message' =>$e->getMessage()
                ],401); 
            }
        }
        $roles = Role::where('name', 'admin')->pluck('description','name');
        $appTypes = AppType::where('is_deleted',0)->get();
        return view('admin.users.store',compact('roles','appTypes'));
    }
    public function edit(Request $request, $id)
    {
        //Update User
        $user = User::find($id);
        $roleUser = UserRole::where('user_id',$id)->get()->first();

        if( $request->isMethod('post') && $request->ajax()){
            $messages = [
              'first_name.required' => 'First Name field is required.',
              'phone.required' => 'Phone field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'phone' => 'required',
                'first_name' => 'required|max:26',
            ],$messages);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            foreach($request->all() as $key=>$value){
                if( in_array( $key,$user->getFillable() ) ){
                    $user->$key = $value;
                }
            } 
            if(isset($request->password) && $request->password !=''){
                $user->password = Hash::make($request->password);
            }  
             
            $user->save();
           	$role_id = Role::where('name', $request->role_name)->first()->id;
            $user->roles()->sync($role_id);
			$user->save();

            $user->appAccesses()->sync($request->app_access);

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Account has been updated.',
                    'redirect_url'  => url(self::REDIRECT_URL)
            ]);
        }
        $roles = Role::where('name', 'admin')->pluck('description','name');
        $appTypes = AppType::where('is_deleted',0)->get();
        return view('admin.users.edit', compact('user','roles','roleUser','appTypes'));
    }
    public function deleteuserall(Request $request)
    {
        $ids = $request->id;
        //dd($ids);
        if(!empty($ids)){
            
            User::whereIn('id',$ids)->delete();
            UserRole::whereIn('user_id',$ids)->delete();
            AppTypeUser::whereIn('user_id',$ids)->delete();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'message'   =>'User(s) deleted successfully.',
                    'redirect_url'   =>true
            ]);
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'message'   =>'Please select at least one checkbox',
                    'redirect_url' =>true
            ],401);
        }        
    }
    public function delete(Request $request, $id)
    {
    	if (User::find($id)){
            $user = User::find($id);
            UserRole::where('user_id',$id)->delete();
            $user->delete();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'message'   =>'Account deleted successfully.',
                    'redirect_url'  => url('users')
            ]);
         }
    }
}
