<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\UserRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {	
        return view('admin.dashboard.index');
    }
}
