<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use DB;
use App\Role;
use App\User;
use App\UserRole;
use App\AppType;
use App\MessageType;
use App\Template;
use App\InsertVariable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SmsTemplateController extends Controller
{
    public const REDIRECT_URL = '/templates';

    public function index(Request $request){

        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;

        if (Auth::user()->roles()->get()->first()->name == 'admin') {

            $templates = Template::where('is_delete',0)->orderBy('created_at','desc')->paginate($current_page);

        }else if (Auth::user()->roles()->get()->first()->name == 'super_admin') {
            
            $templates = Template::where('is_delete',0)->orderBy('created_at','desc')->paginate($current_page);
        }
        $templates->appends($request->all());
        if(isset($request->page)){
            $sr_no_start = ($request->page * $current_page)-$current_page + 1;
        }else{
            $sr_no_start = 1;
        }
        return view('admin.templates.index',compact('templates','current_page','sr_no_start'));
    }
    public function createtemplate(Request $request)
    {
        $messageTypes = MessageType::orderBy('id','DESC')->pluck('name','name');
        $messageTypes->prepend('Choose message type','');
        $appTypes = AppType::where('is_deleted',0)->pluck('name','id');
        $insertVariables = InsertVariable::pluck('name','name');
        
        //Create SMS tenplate
        if( $request->isMethod('post') && $request->ajax()){

            //Start Validation
            $messages = [
              'message.required' => 'Message field is required.',
              'title.required' => 'Name field is required.',
              'message_type.required' => 'Message Type is required.',
            ];
            $validator = Validator::make($request->all(), [
                'message' => 'required',
                'title' => 'required|max:26',
                'message_type' => 'required',
            ],$messages);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            $template = new Template();
            foreach($request->all() as $key=>$value){
                if( in_array( $key,$template->getFillable() ) ){
                    $template->$key = ($key == 'message')?str_replace(['<p>', '</p>'], '', $value):$value;
                }
            }                 
            $template_id = master_unique_code(new DB,'templates','a3k-temp-','template_id');
            $template->template_id =$template_id;
            $template->save();
            return response()->json([
                    'success' => true,
                    'data'    => [],
                    'reload'  => '',
                    'message' => 'Template has been created.',
                    'redirect_url' => url(self::REDIRECT_URL)
            ]);
        }
        return view('admin.templates.store',compact('messageTypes','appTypes','insertVariables'));
    }
    public function edittemplate(Request $request, $id)
    {
        //Update SMS tenplate
        $template = Template::find($id);
        $messageTypes = MessageType::orderBy('id','DESC')->pluck('name','name');
        $messageTypes->prepend('Choose message type','');
        $appTypes = AppType::where('is_deleted',0)->pluck('name','id');
        $insertVariables = InsertVariable::pluck('name','name');

        if( $request->isMethod('post') && $request->ajax()){

            //start validaton
            $messages = [
              'message.required' => 'Message field is required.',
              'title.required' => 'Name field is required.',
              'message_type.required' => 'Message Type is required.',
            ];
            $validator = Validator::make($request->all(), [
                'message' => 'required',
                'title'   => 'required|max:26',
                'message_type' => 'required',
            ],$messages);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            foreach($request->all() as $key=>$value){
                if( in_array( $key,$template->getFillable() ) ){
                    $template->$key = ($key == 'message')?str_replace(['<p>', '</p>'], '', $value):$value;
                }
            } 
            if(trim($request->get ('password')) !=''){
                $template->password = Hash::make ( $request->get ('password') );
            }  
            $template->save();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'message'   =>'Template has been updated.',
                    'reload'  => '',
                    'redirect_url' => url(self::REDIRECT_URL)
            ]);
        }
        return view('admin.templates.edit', compact('template','messageTypes','appTypes','insertVariables'));
    }
   
    public function deletetemplate(Request $request, $id)
    {
        if (Template::find($id)){
            $template = Template::find($id);
            $template->is_delete= 0;
            $template->save();
            return redirect()->back()->with('success', 'Template deleted successfully');
        }else{
            return redirect()->back()->with('error', 'Failed');
        }
    }
    public function deletetemplateall(Request $request)
    {
        $ids = $request->id;
        //dd($ids);
        if(!empty($ids)){
            Template::whereIn('id',$ids)->update(['is_delete' => 1]);
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'message'   =>'Tempaltes deleted successfully.',
                    'redirect_url'   =>true
            ]);
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'message'   =>'Please select at least one checkbox',
                    'redirect_url' =>true
            ],401);
        }        
    }
    public function variablecreate_store(Request $request){

        if( $request->isMethod('post') && $request->ajax()) {

            Validator::extend('without_spaces', function($attr, $value){
                return preg_match('/^\S*$/u', $value);
            });
  
            $validator = Validator::make($request->all(), [
                'variable_name' => 'required|without_spaces|max:20'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            $is_exist = InsertVariable::where('name',$request->variable_name)->first();
            //dd($is_exist);
            if(!empty($is_exist)){
                $insertVariable = InsertVariable::find($is_exist->id);
            }else{                
                $insertVariable = new InsertVariable();
                $insertVariable->name = str_replace(' ', '', $request->variable_name);
            }
            $insertVariable->possible_values = $request->possible_values;
            $insertVariable->save();

            $variables = InsertVariable::get();
            $html = '';
            foreach ($variables as $variable) {
                $html .= '<option value="'.$variable->name.'">'.$variable->name.'</option>';
            }
              
            return response()->json([
                'success' => true,
                'data'   => [],
                'html' => $html,
                'message'   =>'Variable Created successfully.',
                'redirect_url'   =>true
            ]);
        }
        
    }
    public function get_possible_values(Request $request){

        if($request->ajax()) {

            $validator = Validator::make($request->all(), [
                'val' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            $possible_values = InsertVariable::where('name',$request->val)->first()->possible_values;
            return response()->json([
                'success' => true,
                'data'    => $possible_values,
                'message' => 'success',
            ]);
        }
    }
    public function message_type_store(Request $request){

        if($request->isMethod('post') && $request->ajax()) {

            //start validaton
            $validator = Validator::make($request->all(), [
                'other' => 'required|unique:message_type,name|max:50'
            ]);
            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation
             $messageType =  new MessageType();
             $messageType->name = $request->other;
             $messageType->save();

             $messageTypes = MessageType::orderBy('id','DESC')->get();
             $html = '';
             foreach ($messageTypes as $messageType) {
                $html .= '<option value="'.$messageType->name.'">'.$messageType->name.'</option>';
             }
              
             return response()->json([
                'success' => true,
                'data'   => [],
                'html' => $html,
                'message'   =>'Message type created successfully.',
                'redirect_url'   =>true
            ]);
        }            
    }
    public function getTemplatePreview(Request $request){

        if($request->isMethod('post') && $request->ajax()) {

            $id = $request->template_id;
            $template =  Template::find($id);
            //dd($template);
            $view = view('admin.ajax.viewTemplate',compact('template'))->render();           
            return response()->json([
                'success' => true,
                'data'   => [],
                'html' => $view,
                'message'   =>'Message type created successfully.',
                'redirect_url'   =>true
            ]);
        }              
    }
    
}
