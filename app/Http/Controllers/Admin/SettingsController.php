<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use App\User;
use App\Role;
use App\AppType;
use App\UserRole;
use App\MessageType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SettingsController extends Controller
{
    public function app_setting(Request $request){

        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;
        $data = AppType::where('is_deleted',0)->paginate($current_page);
        $data->appends($request->all());    	
        if(isset($request->page)){
            $sr_no_start = ($request->page * $current_page)-$current_page + 1;
        }else{
            $sr_no_start = 1;
        }  
        return view('admin.settings.apptype.index',compact('data','current_page','sr_no_start'));
    }
    public function createapp(Request $request)
    {
        if( $request->isMethod('post') && $request->ajax()){
            $messages = [
              'name.required' => 'Name field is required.',
              'auth_key.required' => 'Auth key field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'auth_key' => 'required',
            ],$messages);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation
            $app = new AppType();
            foreach($request->all() as $key=>$value){
                if( in_array( $key,$app->getFillable() ) ){
                    $app->$key = $value;
                }
            } 
            $app->save();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been created.',
                    'redirect_url'  => url('settings/appsetting')
            ]);
        }
        $random_auth_key = Str::random(20);
        return view('admin.settings.apptype.store',compact('random_auth_key'));
    }
    public function editapp(Request $request, $id)
    {
        $app = AppType::find($id);

        if( $request->isMethod('post') && $request->ajax()){
            $messages = [
              'name.required' => 'Name field is required.',
              'auth_key.required' => 'Auth key field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'auth_key' => 'required',
            ],$messages);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            foreach($request->all() as $key=>$value){
                if( in_array( $key,$app->getFillable() ) ){
                    $app->$key = $value;
                }
            } 
            $app->save();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been updated.',
                    'redirect_url'  => url('settings/appsetting')
            ]);
        }
        return view('admin.settings.apptype.edit', compact('app'));
    }
   
    public function deleteApp(Request $request, $id)
    {
    	if (AppType::find($id)){
            $appTypes = AppType::find($id);
            $appTypes->is_deleted = 1;
            $appTypes->save();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been deleted successfully',
                    'redirect_url'  => url('settings/appsetting')
            ]);
            //return redirect()->back()->with('success', 'Record has been deleted successfully');
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Something error in delete',
                    'redirect_url'  => ''
            ],401);
            //return redirect()->back()->with('error', 'Failed');
        }
    }

    public function message_type_setting(Request $request){
        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;
        $data = MessageType::orderBy('id','desc')->paginate($current_page);
        $data->appends($request->all());  
        if(isset($request->page)){
            $sr_no_start = ($request->page * $page_size)-$page_size + 1;
        }else{
            $sr_no_start = 1;
        }    
        return view('admin.settings.messagetype.index',compact('data','current_page','sr_no_start'));
    }

    public function create_message_type(Request $request)
    {
        if( $request->isMethod('post') && $request->ajax()){

            $messages = [
              'name.required' => 'Name field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ],$messages);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation
            $app = new MessageType();
            foreach($request->all() as $key=>$value){
                if( in_array( $key,$app->getFillable() ) ){
                    $app->$key = $value;
                }
            } 
            $app->save();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been created.',
                    'redirect_url'  => url('settings/messagetypesetting')
            ]);
        }
        return view('admin.settings.messagetype.store');
    }
    public function edit_message_type(Request $request, $id)
    {
        $app = MessageType::find($id);

        if( $request->isMethod('post') && $request->ajax()){

            $messages = [
              'name.required' => 'Name field is required.',
            ];
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ],$messages);

            if ($validator->fails()) {
                return response()->json(['error'=>$validator->errors()], 401);            
            } 
            //end Validation

            foreach($request->all() as $key=>$value){
                if( in_array( $key,$app->getFillable() ) ){
                    $app->$key = $value;
                }
            } 
            $app->save();

            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been updated.',
                    'redirect_url'  => url('settings/messagetypesetting')
            ]);
        }
        return view('admin.settings.messagetype.edit', compact('app'));
    }

    public function delete_message_type(Request $request, $id)
    {
        if (MessageType::find($id)){
            MessageType::where('id',$id)->delete();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been deleted successfully',
                    'redirect_url'  => url('settings/messagetypesetting')
            ]);
            //return redirect()->back()->with('success', 'Record has been deleted successfully');
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Something error in delete',
                    'redirect_url'  => ''
            ],401);
            //return redirect()->back()->with('error', 'Failed');
        }
    }


}
