<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use DB;
use App\Role;
use App\User;
use App\UserRole;
use App\Template;
use App\SendSms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Illuminate\Support\Str;
use View;
use File;
use Response;

class SendSMSController extends Controller
{
    public const REDIRECT_URL = 'sms/sendmessage';

    public function store(Request $request)
    {
        date_default_timezone_set("Asia/Kolkata");
        $templates = Template::where('is_delete',0)->pluck('title','template_id');
        $templates->prepend('Choose the existing template','');

        if( $request->isMethod('post') && $request->ajax()){
            
            if(isset($request->template_id)){
                $templateRow = Template::where('template_id',$request->template_id)->first();
                $app_id   = $templateRow->choose_app;
            }else{
                $app_id   = '';
            }
            if($request->hasFile('file'))
            { 
                //with csv
                //Start Validation
                $send_type = $request->send_type;
                
                if($send_type == 'scheduled'){

                    $messages = [
                      'message.required'        => 'Message field is required.',
                      //'phone_number.required'   => 'Phone number field is required.',
                      'schedule_time3.required' => 'Schedule date field is required.',
                      'schedule_time4.required' => 'Schedule time field is required.',
                      'uploadCSV.required'      => 'CSV field is required.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'message'        => 'required',
                        //'phone_number'   => 'required',
                        'schedule_time3' => 'required',
                        'schedule_time4' => 'required',
                        'uploadCSV'      => 'required',
                    ],$messages);
 
                }else{

                    $messages = [
                      'uploadCSV.required'        => 'CSV field is required.',
                      'message.required'          => 'Message field is required.',
                      'csv_phone_number.required' => 'Phone number mapping field is required.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'uploadCSV.required' => 'CSV field is required.',
                        'message'            => 'required',
                        'csv_phone_number'   => 'required',
                    ],$messages);
                }
                if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);            
                }

                $dynamic_variables  = $request->dynamic; 
                $message            = $request->message;
                $csv_phone_header   = $request->csv_phone_number;

                $path = $request->file('file')->getRealPath();
                $csvdata = array_map('str_getcsv', file($path));

                if(!empty($csvdata)){

                    array_walk($csvdata, function(&$a) use ($csvdata) {
                      $a = array_combine($csvdata[0], $a);
                    });
                    array_shift($csvdata); 

                    $csv_headers = array_column($dynamic_variables, 'csv_headers');
                    array_push($csv_headers, $csv_phone_header);
                    array_unique($csv_headers);                
                    $keys = array_flip($csv_headers);

                    $filteredArray = array_map(function($a) use($keys){
                        return array_intersect_key($a,$keys);
                    }, $csvdata); 
                    $msg = array();
                    $pattern = '##%s##';
                    foreach ($filteredArray as $row_id => $row) {
 
                        $recipient = $row[$csv_phone_header];
                        $is_valid_row = 1;
                        
                        if(!empty($row)){
                            foreach($row as $key=>$val){
                                if($val == ''){
                                    $is_valid_row = 0;//send only for valid rows
                                }
                                foreach($dynamic_variables as $sourceKey=>$sourceValue){
                                    if($sourceValue['custom_val'] !=''){
                                        $varMap[sprintf($pattern,$sourceValue['template_variables'])] = $sourceValue['custom_val'];
                                    }else{
                                        if($key == $sourceValue['csv_headers']){
                                            $varMap[sprintf($pattern,$sourceValue['template_variables'])] = $val;
                                        }
                                    }
                                }
                            }
                            $new_message = strtr($message,$varMap);
                        }else{
                            $new_message = $message;
                        } 
                        //dd($is_valid_row);
                        if($is_valid_row){
                        
                            if($send_type == 'one_time'){

                                $response = sendMessage($recipient, $new_message);
                                $response_arr = json_decode($response,true);
                                $msg[] = $response_arr;
                                $is_sent = (!empty($response_arr) && ($response_arr['sid']>0))?1:0;
                                $schedule_time = NULL;

                            }else{

                                //scheduled
                                $is_sent = 0;
                                $schedule_time = $request->schedule_time3.' '.$request->schedule_time4;
                                $response_arr = array();
                                $msg[] = array('success'=>true,'message'=>'Your message has been scheduled successfully.');
                            }    
                            
                            //save to db
                            $sendsms = new SendSms();
                            $sendsms->phone_numbers = $recipient;
                            $sendsms->template_id   = $request->template_id;
                            $sendsms->message       = $new_message;
                            $sendsms->send_type     = $send_type;
                            $sendsms->schedule_time = $schedule_time;
                            $sendsms->is_sent       = $is_sent;
                            $sendsms->created_by    = Auth::id();
                            $sendsms->app_id        = $app_id;
                            $sendsms->send_source   = 'Web';
                            $sendsms->sid           = !empty($response_arr)?$response_arr['sid']:'';
                            $sendsms->response      = !empty($response_arr)?$response_arr['message']:'';             
                            $sendsms->save();
                        }
                    }
                }
                return response()->json([
                    'success'       => true,
                    'data'          => [],
                    'message'       => $msg,
                    'reload'        => '',
                    'redirect_url'  => url(self::REDIRECT_URL)
                ]);

            }else{//without csv

                //Start Validation
                $send_type = $request->send_type;
                
                if($send_type == 'scheduled'){

                    $messages = [
                      'message.required'        => 'Message field is required.',
                      'phone_number.required'   => 'Phone number field is required.',
                      'schedule_time.required'  => 'Schedule date field is required.',
                      'schedule_time2.required' => 'Schedule time field is required.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'message'        => 'required',
                        'phone_number'   => 'required',
                        'schedule_time'  => 'required',
                        'schedule_time2' => 'required',
                    ],$messages);
 
                }else{

                    $messages = [
                      'message.required'        => 'Message field is required.',
                      'phone_number.required'   => 'Phone number field is required.',
                    ];
                    $validator = Validator::make($request->all(), [
                        'message'       => 'required',
                        'phone_number'  => 'required',
                    ],$messages);
                }
                if ($validator->fails()) {
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
                //dd($request->all());            
                $message    = $request->message;
                $pattern = "/\\##(.*?)\\##/";
                
                if (preg_match($pattern, $message)) {
                    return response()->json(['error'=>[['Please replace dynamic variables before send']]], 401);  
                }
                //end Validation
                $recipients = explode(',', $request->phone_number);
                /*if($request->schedule_time !=''){
                    $send_type = 'scheduled';
                    $schedule_time = $request->schedule_time.' '.$request->schedule_time2;
                }else{
                    $send_type = 'one_time';
                }*/
                $msg = array();
                foreach($recipients as $recipient) {
                    
                    if($send_type == 'one_time'){

                        $response = sendMessage($recipient, $message);
                        $response_arr = json_decode($response,true);
                        $msg[] = $response_arr;
                        $is_sent = (!empty($response_arr) && ($response_arr['sid']>0))?1:0;
                        $schedule_time = NULL;
                    }else{

                        //scheduled
                        $schedule_time = $request->schedule_time.' '.$request->schedule_time2;
                        $is_sent = 0;
                        $response_arr = array();
                        $msg[] = array('success'=>true,'message'=>'Your message has been scheduled successfully.');
                    }    
                    
                    //save to db
                    $sendsms = new SendSms();
                    $sendsms->phone_numbers = $recipient;
                    $sendsms->template_id   = $request->template_id;
                    $sendsms->message       = $message;
                    $sendsms->send_type     = $send_type;
                    $sendsms->schedule_time = $schedule_time;
                    $sendsms->is_sent       = $is_sent;
                    $sendsms->created_by    = Auth::id();
                    $sendsms->app_id        = $app_id;
                    $sendsms->send_source   = 'Web';
                    $sendsms->sid           = !empty($response_arr)?$response_arr['sid']:'';
                    $sendsms->response      = !empty($response_arr)?$response_arr['message']:'';                
                    $sendsms->save();
                }
                
                return response()->json([
                        'success'       => true,
                        'data'          => [],
                        'message'       => $msg,
                        'reload'        => '',
                        'redirect_url'  => url(self::REDIRECT_URL)
                ]);
            }
        }
        return view('admin.sendsms.store',compact('templates'));
    }
    
    public function ajaxUploadCSV(Request $request){

        if($request->hasFile('file'))
        {
            $image = $request->file('file');
            //$file_name = 'csv_'.time().'.'.$image->getClientOriginalExtension();

            if($image->getClientOriginalExtension() == 'csv'){

                $path = $request->file('file')->getRealPath();
                $data = array_map('str_getcsv', file($path));
                //dd($data);
                $phone_array = array();
                if(!empty($data)){
                    foreach ($data as $key => $value) {

                        if($key == 0) continue;
                        array_push($phone_array, $value[0]);
                    }
                    return response()->json([
                    'success' => true,
                    'data'    => !empty($phone_array)?implode(',', $phone_array):'',
                    'message' =>'File uploaded successfully.',
                    ],200);
                }else{
                    return response()->json(['error'=>[['CSV have no data to upload']]], 401);
                }                
            }else{
                return response()->json(['error'=>[['Invalid file type']]], 401);
            }
        }          
    }
    public function get_template_data(Request $request){

        $id = $request->template_id;
        $template = Template::where('template_id',$id)->first();
        $message = str_replace('&nbsp;', '', $template->message);
        $dynamic_value_html = '';

        if($request->hasFile('file'))
        {
            $image = $request->file('file');
            
            if($image->getClientOriginalExtension() == 'csv'){

                $path = $request->file('file')->getRealPath();
                $csvdata = array_map('str_getcsv', file($path));

                $matches = array();
                preg_match_all("/##.+?##/", $message, $matches);

                if(!empty($matches)){
                    $csv_headers = $csvdata[0];                    
                    $variables   = array_unique($matches[0]);
                    //dd($csv_headers);
                    $dynamic_value_html = view('admin.sendsms.dynamic_value',compact('variables','csv_headers','template'))->render();
                }

                //$phone_array = array();
                if(!empty($csvdata)){
                    /*foreach ($csvdata as $key => $value) {

                        if($key == 0) continue;
                        array_push($phone_array, $value[0]);
                    }*/
                    return response()->json([
                        'success' => true,
                        'data'    => $message,//!empty($phone_array)?implode(',', $phone_array):'',
                        'dynamic_value_html' => $dynamic_value_html,
                        'message' => 'success',
                    ],200);
                }else{
                    return response()->json(['error'=>[['CSV have no data to upload']]], 401);
                }                
            }else{
                return response()->json(['error'=>[['Invalid file type']]], 401);
            }
        }
        return response()->json([
            'success' => true,
            'data'    => $message,
            'dynamic_value_html' => $dynamic_value_html,
            'message' =>'success',
        ]);
    }

    public function check_variables_mapping(Request $request){

        $error_arr = array();
        $error_str = '';
        $dynamic_variables = $request->dynamic;
        $send_type         = $request->send_type;

        $i = 0;

        foreach ($dynamic_variables as $key => $val) {
            if($val['csv_headers'] == 'custom_value' && $val['custom_val'] == ''){
                $error_arr[] = '<li class="text-danger">'.$val['template_variables'].'</li>';
            }else if($val['csv_headers'] == ''){
                $error_arr[] = '<li class="text-danger">'.$val['template_variables'].'</li>';
            }
            $i++;
        }
        if(count($error_arr)>0){

            return response()->json([
                    'success' => false,
                    'data'    => implode('',$error_arr),
                ]);

        }else{

            if($request->hasFile('file'))
            {
                $image = $request->file('file');
                $csv_headers = array_column($dynamic_variables, 'csv_headers');
                $csv_phone_number = $request->csv_phone_number;
                array_push($csv_headers, $csv_phone_number);
                array_unique($csv_headers);                
                $keys = array_flip($csv_headers);

                if($image->getClientOriginalExtension() == 'csv'){

                    $path = $request->file('file')->getRealPath();
                    $csvdata = array_map('str_getcsv', file($path));
                    
                    if(!empty($csvdata)){

                        array_walk($csvdata, function(&$a) use ($csvdata) {
                          $a = array_combine($csvdata[0], $a);
                        });
                        array_shift($csvdata); 
                        
                        $filteredArray = array_map(function($a) use($keys){
                            return array_intersect_key($a,$keys);
                        }, $csvdata);
                        //dd($filteredArray);
                        $error_detail  = '';
                        $processed_row = count($csvdata);
                        $invalid_row   = 0;
                        $sr_no = 1;
                        foreach ($filteredArray as $row_id => $row) {
                            $temp_counter = 0;
                            foreach ($row as $key => $val) {
                                if($val == ''){
                                    $temp_counter++;
                                    $error_detail .= '<div class="rowitem"><div>'.$sr_no.'.</div><div>Row ID: '.($row_id+2).'<br/>
                                                      CSV Header: '.$key.' <br/>
                                                      Issue: There is no value in '.$key.' Column </div></div>';

                                    $error_str .= $sr_no.'.Row ID: '.($row_id+2).PHP_EOL;
                                    $error_str .= 'CSV Header: '.$key.PHP_EOL;
                                    $error_str .= 'Issue: There is no value in '.$key.' Column'.PHP_EOL;
                                    $sr_no++;
                                }
                            }
                            if($temp_counter>0){
                                $invalid_row++;
                            }
                        }
                        $valid_row = $processed_row - $invalid_row;

                        File::put(public_path('/download/error_log.txt'),$error_str);
                        
                        $error_html = view('admin.sendsms.preview_error',compact('processed_row','valid_row','invalid_row','error_detail','send_type'))->render();

                        return response()->json([
                                'success'  => true,
                                'data'     => $error_html,
                                'error'    => $error_str,
                            ]);

                    }else{

                        return response()->json([
                            'success' => false,
                            'data'    => '<li>CSV file have no records</li>',
                            'error'   => $error_str,
                        ]);
                    }

                }else{

                   return response()->json([
                        'success' => false,
                        'data'    => '<li>Invalid CSV file</li>',
                        'error'   => $error_str,
                    ]); 
                }
            }else{

                return response()->json([
                    'success' => false,
                    'data'    => '<li>Please choose CSV file</li>',
                    'error'   => $error_str,
                ]);
            }
        }
    }
    public function download_error_logs(Request $request){

        $content  = $request->log_text; 
        return response()->attachment($content);
    }
}
