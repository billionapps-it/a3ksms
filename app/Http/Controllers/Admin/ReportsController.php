<?php

namespace App\Http\Controllers\Admin;

use Hash;
use Auth;
use App\User;
use App\Role;
use App\SendSms;
use App\AppType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Exports\DeliveryReportsExport;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public function delivery_reports(Request $request){

        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;
        $from_date = '';
        $to_date   = '';
        $app_id    = '';
        $selected_source = '';

        $collection = SendSms::select('send_sms.*');     

        if(isset($request->app_id) && $request->app_id >0){
        	$app_id = $request->app_id;
			$collection->where('send_sms.app_id', $app_id);
        }

        if(isset($request->message_source)){

        	$selected_source = $request->message_source;
            $collection->where('send_sms.send_source', $selected_source);
        	/*if($request->message_source == 'Web'){
				$collection->where('send_sms.app_id', NULL);
			}else if($request->message_source == 'API'){
				$collection->where('send_sms.app_id', '>=', 0);
			}*/
        }

        if(isset($request->from_date) && isset($request->to_date)){
        	$from_date = $request->from_date;
        	$to_date   = $request->to_date;
        	$collection->whereBetween('send_sms.created_at', [$from_date.' 00:00:00', $to_date.' 23:59:59']);
        }elseif(isset($request->from_date)){
            $from_date = $request->from_date;
            $collection->whereDate('send_sms.created_at', $from_date);
        }elseif(isset($request->to_date)){
            $to_date   = $request->to_date;
            $collection->whereDate('send_sms.created_at', $to_date);
        }

        $collection->orderBy('send_sms.created_at','desc');
        $data = $collection->paginate($current_page);
        $data->appends($request->all());
        $app_types = AppType::where('is_deleted',0)->pluck('name','id');
        $app_types->prepend('All',''); 
        $message_sources = [
        					''    => 'All',
        					'Web' => 'Web',
        					'API' => 'API'
        					];	
        if(isset($request->page)){
            $sr_no_start = ($request->page * $current_page)-$current_page + 1;
        }else{
            $sr_no_start = 1;
        }
        return view('admin.reports.delivery_reports',compact('data','current_page','app_types','message_sources','from_date','to_date','app_id','selected_source','sr_no_start'));
    }
    public function export(Request $request) 
    {
	   $inputArr 	= request()->except(['_token']);	   
       return Excel::download(new DeliveryReportsExport($inputArr), 'Delivery_Reports_'.time().'.xlsx');
    }
    public function scheduled_reports(Request $request){

        $page_size = 5;
        $current_page = isset($request->item)?$request->item:$page_size;
        $from_date = '';
        $to_date   = '';
        $app_id    = '';
        $selected_source = '';

        $collection = SendSms::select('send_sms.*')
                      ->where('send_sms.send_type','scheduled')    
                      ->where('send_sms.is_sent',0)
                      ->where('send_sms.is_cancelled',0)
                      ->where('send_sms.is_deleted',0);

        if(isset($request->app_id) && $request->app_id >0){
            $app_id = $request->app_id;
            $collection->where('send_sms.app_id', $app_id);
        }

        if(isset($request->message_source)){

            $selected_source = $request->message_source;
            $collection->where('send_sms.send_source', $selected_source);
            /*if($request->message_source == 'Web'){
                $collection->where('send_sms.app_id', NULL);
            }else if($request->message_source == 'API'){
                $collection->where('send_sms.app_id', '>=', 0);
            }*/
        }

        if(isset($request->from_date) && isset($request->to_date)){
            $from_date = $request->from_date;
            $to_date   = $request->to_date;
            $collection->whereBetween('send_sms.created_at', [$from_date.' 00:00:00', $to_date.' 23:59:59']);
        }elseif(isset($request->from_date)){
            $from_date = $request->from_date;
            $collection->whereDate('send_sms.created_at', $from_date);
        }elseif(isset($request->to_date)){
            $to_date   = $request->to_date;
            $collection->whereDate('send_sms.created_at', $to_date);
        }

        $collection->orderBy('send_sms.created_at','desc');
        $data = $collection->paginate($current_page);
        $data->appends($request->all());
        $app_types = AppType::where('is_deleted',0)->pluck('name','id');
        $app_types->prepend('All',''); 
        $message_sources = [
                            ''    => 'All',
                            'Web' => 'Web',
                            'API' => 'API'
                            ];  
        if(isset($request->page)){
            $sr_no_start = ($request->page * $current_page)-$current_page + 1;
        }else{
            $sr_no_start = 1;
        }
        return view('admin.reports.scheduled_reports',compact('data','current_page','app_types','message_sources','from_date','to_date','app_id','selected_source','sr_no_start'));
    }
    public function cancel_report(Request $request, $id)
    {
        if (SendSms::find($id)){
            $appTypes = SendSms::find($id);
            $appTypes->is_cancelled = 1;
            $appTypes->save();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been cancelled successfully',
                    'redirect_url'  => url('reports/scheduled_reports')
            ]);
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Something error in delete',
                    'redirect_url'  => ''
            ],401);
        }
    }
    public function delete_report(Request $request, $id)
    {
        if (SendSms::find($id)){
            $appTypes = SendSms::find($id);
            $appTypes->is_deleted = 1;
            $appTypes->save();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been deleted successfully',
                    'redirect_url'  => url('reports/scheduled_reports')
            ]);
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Something error in delete',
                    'redirect_url'  => ''
            ],401);
        }
    }
    public function re_schedule(Request $request)
    {
        $id = $request->report_id;        
        if (SendSms::find($id)){
            $appTypes = SendSms::find($id);
            $appTypes->schedule_time = $request->schedule_time.' '.$request->schedule_time2;
            $appTypes->save();
            return response()->json([
                    'success' => true,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Record has been updated successfully',
                    'redirect_url'  => url('reports/scheduled_reports')
            ]);
        }else{
            return response()->json([
                    'success' => false,
                    'data'   => [],
                    'reload'  => '',
                    'message'   =>'Something error in delete',
                    'redirect_url'  => ''
            ],401);
        }
    }
}
