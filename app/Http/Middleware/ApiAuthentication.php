<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   //apache_request_headers()
        $token = $request->header('authorization-token');
        //if ($token != "3MPHJP0BC63435345341") {
        if ($token == '') {
            return response()->json(['message'=>'Authorization-token is required'],401);
        }elseif($token != ''){
            $valid = \App\AppType::where('auth_key',$token)->get();
            if(count($valid)){
                return $next($request);
            }else{
                return response()->json(['message'=>'Invalid Token!'],401);
            }
        }
        return $next($request);
    }
}
