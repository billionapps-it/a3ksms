<?php
namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AdminAuthentication extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::check() && Auth::user()->roles()->get()->first()->name == 'admin') {

            if(! $request->ajax()){
                $not_premissions_url = array('users','user/edit','user/store','user/delete');
                if (strpos($request->path(), 'edit') !== false || strpos($request->path(), 'delete') !== false
                ) {
                    $url = trim(preg_replace('/[0-9]+/', '', $request->path()),'/');
                }else{
                    $url = $request->path();
                }
                if(in_array($url, $not_premissions_url)){
                    abort(404);
                }else{
                    return $next($request);
                }
            }
            return $next($request);
        }else if (Auth::check() && Auth::user()->roles()->get()->first()->name == 'super_admin') {
            return $next($request);
        }else{
            return redirect('login');  
        }
    }
}
