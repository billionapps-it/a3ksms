<?php
namespace App\Http\Middleware;
use Auth;
use Closure;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class SuperAdminAuthentication extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { dd(Auth::user()->roles()->get()->first()->name);
        if (Auth::check() && Auth::user()->roles()->get()->first()->name == 'super_admin') {
            return $next($request);
        }else if (Auth::check() && Auth::user()->roles()->get()->first()->name == 'admin') {
            return $next($request);
        }else{
            abort(401,'Unauthorized access');    
        }
    }
}
