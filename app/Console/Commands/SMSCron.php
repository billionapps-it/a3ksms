<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SendSms;
use DB;

class SMSCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        date_default_timezone_set('Asia/Kolkata');
        //\Log::info("Cron is working fine!");      
        $current_time = date('Y-m-d H:i');
        $from_time = $current_time.':00';
        $to_time   = $current_time.':59';
        \Log::info("Cron is working fine!".$current_time);
        //DB::enableQueryLog(); // Enable query log
        $schedules = SendSms::where('send_type','scheduled')
                    ->where('is_sent',0)
                    ->where('send_sms.is_cancelled',0)
                    ->where('send_sms.is_deleted',0)
                    ->whereBetween("schedule_time",[$from_time,$to_time])
                    ->orderBy('id','asc')
                    ->get();
        //$sql = "SELECT * FROM send_sms WHERE send_type = 'scheduled' AND is_sent=0 AND (schedule_time BETWEEN '".$from_time."' AND '".$to_time."')";
        //$results = DB::select(DB::raw("SELECT * FROM send_sms WHERE send_type = 'scheduled' AND is_sent=0 AND (schedule_time schedule_time BETWEEN $from_time AND $to_time)"));            
        //dd(DB::getQueryLog()); // Show results of log
        //\Log::info("Data",array($sql));
        //$this->info('Data:',$sql);
        if(count($schedules)>0){
            foreach($schedules as $schedule){
                $recipient  = $schedule->phone_numbers;
                $message    = $schedule->message;
                $response   = sendMessage($recipient, $message);
                $response_arr = json_decode($response,true);
                $is_sent = !empty($response_arr)?1:0;
                $sid     = !empty($response_arr)?$response_arr['sid']:'';
                $res = !empty($response_arr)?$response_arr['message']:'';
                SendSms::where('id',$schedule->id)->update(['is_sent'=>$is_sent,'sid'=>$sid,'response'=>$res]);
                $this->info('Executed id:'.$schedule->id);
            }
        }
    }
}
