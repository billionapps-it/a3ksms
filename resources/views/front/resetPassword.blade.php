<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Reset Password | Achieve3000</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/brand.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/responsive.css')}}">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/backend/css/toastr.min.css') }}"> 
</head>
<body>
  <div class="container-fluid">
    <header class="head-section">
      <nav class="navbar navbar-expand-sm">
        <div class="container">
          <!-- <a class="navbar-brand" href="#">Navbar</a> -->
          <img src="{{ asset('public/images/logo.svg')}}" class="header-logo">

          <ul class="navbar-nav login ml-auto">
            <li class="nav-item">
              <a href="{{ url('login') }}" class="btn my-2 my-sm-0" type="submit">Login</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
     <div class="container">
      <section class="login-page">
        <div class="row justify-content-center">
          <div class="col-sm-6">
            <section class="login-container">
              <div class="head text-center">
                <p>Reset Password</p>
              </div>
              <div class="body-container">
                @if( session('success'))
                  <div class="alert alert-success pt10 pb10 mt10 mb10">
                    {{ session('success') }}
                  </div>
                @endif

                @if( session('error'))
                  <div class="alert alert-danger mt10 mb10  pt10 pb10">
                    {{ session('error') }}
                  </div>
                @endif
                {{ Form::open(array('url' =>'', 'files' => true, 'class' => '','id'=>'resetPasswordForm')) }}
    
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Password <span class="star">*</span></label>
                        <input type="password" name="password" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Confirm Password <span class="star">*</span></label>
                        {{ Form::password('password_confirmation',array('class'=>'form-control' )) }}
                      </div>
                    </div>
                  </div>
    
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="">
                          <button type="submit" class="btn btn-primary my-4">Update Password</button>
                        </div>
                      </div>
                    </div>
                  </div>    
                {{ Form::close() }}
            </section>
          </div>
        </div>
      </section>
    </div>
    <footer class="footer-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-logo">
            <img src="{{ asset('public/images/logo copy 2.png')}}" class="">
          </div>
          <div class="col-md-9">
            <div class="d-flex flex-row">
            <div class="ml-auto">
                <a href="https://www.facebook.com/pages/Achieve3000/333675157246" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-facebook-f'></i></div></a>
                <a href="https://www.linkedin.com/company/achieve3000/" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-linkedin-in'></i></div></a>
                <a href="https://twitter.com/achieve3000" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-twitter'></i></div></a>
              </div>
            </div>
            <div class="d-flex justify-content-end">
            <ul class="un-bull">
              <li><a href="https://www.achieve3000.com/who-we-are/about/">About Us</a></li>
              <span class="vline">|</span> 
              <li><a href="https://achieve3000.applicantstack.com/x/openings">Careers</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/contact-us/">Contact Us</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/press-room/">Press Room</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/privacy-policy/">Privacy Policy</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/terms-use/">Terms of Use</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/terms-of-service/">Terms of Service</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/system-requirements-2020/">System Requirement</a></li>
            </ul>
            </div>
            <div class="d-flex justify-content-end">
              <p class="copyright">Copyright 2020 Achieve3000 | All rights reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!-- JS -->
  <script type="text/javascript" src="{{ asset('public/js/jquery-3.5.1.min.js')}}" ></script>
  <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js')}}" ></script>
  <script src="{{ asset('public/vendor/jquery-validation/dist/jquery.validate.js') }}"></script>
  <script src="{{ asset('public/vendor/jquery-validation/dist/additional-methods.js') }}"></script>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <script src="{{ asset('public/backend/js/toastr.min.js') }}"></script>
 <script>
  $(document).ready(function(){
      jQuery.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
      }
    });
    
    $("#resetPasswordForm").on("submit",function(){
      $.ajax({
          url: "{{ url('resetpassword/'.$token) }}",
          type: 'POST',
          data: $('#resetPasswordForm').serialize(),
          success:function(response){
            if (response.success) {
              toastr.success(response.message,'Success');
              if(response.redirect_url !=''){
                location.href = response.redirect_url;
              }
            }else{
            toastr.error(response.message,'error');
            }
        },  
        error:function(status,error){
          var errors = JSON.parse(status.responseText);
          console.log(status.status);
          if(status.status == 401){
                      $.each(errors.error, function(i,v){ 
              toastr.error( v[0],'Opps!');
            }); 
          }else{
            toastr.error(errors.message,'Opps!');
          }         
              }
      }); 
      return false;
    });
  });
  </script>
</body>
</html>