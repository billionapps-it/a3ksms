@foreach($notificaitons as $notificaiton) <?php //dd(json_decode($notificaiton->data)); ?>
<li href="javascript:void(0);" class="dropdown-item notify-item active">
  <div class="notify-icon">
    <img src="{{ isset($notificaiton->order->item->media['full']) ? url($notificaiton->order->item->media['full']) : '' }} " class="img-fluid rounded-circle" alt="" />
  </div>
  <?php $data = json_decode($notificaiton->data); ?>
  <?php if(!empty($data)){ ?>
  	<p class="notify-details">{{ str_limit($notificaiton->order->item_name, 20) }} 
  	<?php if(isset($data->click_action)){ ?>
  	   <a href="{{ $data->click_action }}?np_hash={{ $notificaiton->id }}" class="btn btn-primary btn-xs" >Read</a>
    <?php } ?>
  </p>
  <?php } ?>
  <p class="text-muted mb-0 user-msg">
    <small>{{ $notificaiton->created_at->diffForHumans() }}</small>
  </p>
</li>
@endforeach
