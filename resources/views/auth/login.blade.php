<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/brand.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/style.css')}}">
  <link rel="stylesheet" href="{{ asset('public/css/responsive.css')}}">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" />
  <title>Login | Achieve3000</title>
</head>
<body>
  <div class="container-fluid">
    <header class="head-section">
      <nav class="navbar navbar-expand-sm">
        <div class="container">
          <!-- <a class="navbar-brand" href="#">Navbar</a> -->
          <img src="{{ asset('public/images/logo.svg')}}" class="header-logo">
          <ul class="navbar-nav login ml-auto">
            <li class="nav-item">
              <a href="{{ url('login') }}" class="btn my-2 my-sm-0">Login</a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
     <div class="container">
      <section class="login-page">
        <div class="row justify-content-center">
          <div class="col-sm-6">
            <section class="login-container">
              <div class="head text-center">
                <p>Login</p>
              </div>
              <div class="body-container">
                @if( session('success'))
                  <div class="alert alert-success pt10 pb10 mt10 mb10">
                    {{ session('success') }}
                  </div>
                @endif

                @if( session('error'))
                  <div class="alert alert-danger mt10 mb10  pt10 pb10">
                    {{ session('error') }}
                  </div>
                @endif
                <form method="POST" action="{{ url('login/action') }}" id="loginForm"> 
                  @csrf
                  <div class="form-group row pad-mid">
                    <div class="col-lg-2">
                      <label for="userName">Username</label>
                    </div>
                    <div class="col-sm-12 col-lg-8 mr-3">
                      <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" aria-describedby="emailHelp" name="email" value="{{ old('email') }}">
                      @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('email') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                  <div class="form-group row pad-15 pad-mid">
                    <div class="col-lg-2">
                      <label for="passWord">Password</label>
                    </div>
                    <div class="col-sm-12 col-lg-8 mr-3">
                      <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" aria-describedby="passwordHelp">
                      @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                  <div class="form-group row pad-0">
                    <div class="col-lg-2"></div>
                    <div class="col-sm-12 col-lg-8 mr-3">
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" name="remember_me" value="TRUE" id="gridCheck">
                          <label class="form-check-label login-check" for="gridCheck">
                            &nbsp; &nbsp; Remember me
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row pad-0">
                    <div class="col-lg-2"></div>
                    <div class="col-sm-12 col-lg-8 mr-3">
                      <button class="btn btn-danger my-2 my-sm-0" type="submit">Login</button>
                    </div>
                  </div>
                </div>
              </form>
            </section>
          </div>
        </div>
      </section>
    </div>
    <footer class="footer-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-3 footer-logo">
            <img src="{{ asset('public/images/logo copy 2.png')}}" class="">
          </div>
          <div class="col-md-9">
            <div class="d-flex flex-row">
              <div class="ml-auto">
                <a href="https://www.facebook.com/pages/Achieve3000/333675157246" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-facebook-f'></i></div></a>
                <a href="https://www.linkedin.com/company/achieve3000/" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-linkedin-in'></i></div></a>
                <a href="https://twitter.com/achieve3000" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-twitter'></i></div></a>
              </div>
            </div>
            <div class="d-flex justify-content-end">
            <ul class="un-bull">
              <li><a href="https://www.achieve3000.com/who-we-are/about/">About Us</a></li>
              <span class="vline">|</span> 
              <li><a href="https://achieve3000.applicantstack.com/x/openings">Careers</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/contact-us/">Contact Us</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/press-room/">Press Room</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/privacy-policy/">Privacy Policy</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/terms-use/">Terms of Use</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/who-we-are/about/terms-of-service/">Terms of Service</a></li>
              <span class="vline">|</span> 
              <li><a href="https://www.achieve3000.com/system-requirements-2020/">System Requirement</a></li>
            </ul>
            </div>
            <div class="d-flex justify-content-end">
              <p class="copyright">Copyright 2020 Achieve3000 | All rights reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <!-- JS -->
  <script type="text/javascript" src="{{ asset('public/js/jquery-3.5.1.min.js')}}" ></script>
  <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js')}}" ></script>
  <script src="{{ asset('public/vendor/jquery-validation/dist/jquery.validate.js') }}"></script>
  <script src="{{ asset('public/vendor/jquery-validation/dist/additional-methods.js') }}"></script>
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
  <script type="text/javascript" src="{{ asset('public/js/custom.js') }}" ></script>
  <style>
    #loginForm .error {
    color: #ff0000;
    }
  </style>
<script>
 $(document).ready(function(){

  setTimeout(function() { $(".alert").hide(); }, 2000);

  jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "No space please and don't leave it empty");

  jQuery.validator.addMethod("laxEmail", function(value, element) {
    // allow any non-whitespace characters as the host part
    return this.optional( element ) || /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test( value );
  }, 'Please enter a valid email address.');
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[id='loginForm']").validate({
    // Specify validation rules
    rules: {
      email: {
        required: true,
        email: true,
        laxEmail:true
      },
      password: {
        required: true,
        noSpace: true
      }
    },
    // Specify validation error messages
    messages: {
      password: {
        required: "Please provide a password",
        noSpace: "White space not allowed in password"
      },
      email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
</body>
</html>