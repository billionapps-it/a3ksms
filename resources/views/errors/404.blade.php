@extends('admin.layouts.index')
@section('content')
<h1>404</h1>
<p>Sorry, You're not able to access this page.</p>
<a class="button" href="{{ url()->previous() }}"><i class="icon-home"></i> Go back</a>
@endsection