@extends('admin.layouts.default')
@section('pageTitle', 'Schedule SMS Reports')
@section('content')
<div class="container ct-pad">
  <section class="mt template">
    <div class="head">
      <h3><img src="{{ asset('public/images/Icons/Manage Templates/Manage-template.svg') }}"> Schedule SMS Reports</h3>
    </div>
    <div class="body-container"> 
      <section>
        {{ Form::open(array('url' => 'reports/scheduled_reports', 'method'=>'get', 'class' => 'section-text','id' => 'export_form')) }}
          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="from_date">From Date</label>
              {{Form::text('from_date',$from_date,array('class'=>'form-control datepickeronly','id'=>'from_date','data-date-format'=>'yyyy-mm-dd', 'autocomplete'=>'off')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="to_date">To Date</label>                   
              {{Form::text('to_date',$to_date,array('class'=>'form-control datepickeronly','id'=>'to_date','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="app_id">App Type</label>                   
              {{Form::select('app_id',$app_types,$app_id,array('class'=>'custom-select','id'=>'app_id')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="message_source">Message Source</label>                   
              {{Form::select('message_source',$message_sources,$selected_source,array('class'=>'custom-select','id'=>'message_source')) }}
            </div>
            <div class="form-group col-md-4 text-right reports_btns">                  
              <button type="submit" class="btn btn-link btn-blue-fill" style="margin-top: 22px;">Search</button>
                <div class="d-inline">       
            {{ Form::close() }} 
            <!--{{ Form::open(array('url' => 'reports/export','method'=>'POST','id'=>'export_for_reports')) }}
              <a href="javascript:void(0)" class="btn btn-link btn-blue-fill mr-none" id="exportExcelReports" style="margin-top: 22px;"><i class="fa fa-file-excel-o"></i> Export</a>
              {{ Form::hidden('exp_from_date',null,array('id'=>'exp_from_date')) }}
              {{ Form::hidden('exp_to_date',null,array('id'=>'exp_to_date')) }}
              {{ Form::hidden('exp_app_id',null,array('id'=>'exp_app_id')) }}
              {{ Form::hidden('exp_message_source',null,array('id'=>'exp_message_source')) }}
            {{ Form::close() }}-->
        </div>
            </div>
          </div> 
      </section>
       @if(count($data)>0)
      <section>
        <div class="form-group pad-bottom col-sm-12">
          <div class="mt-list">
          <table class="table table-striped table-bordered table-hover text-center">
            <thead class="thead-dark">
              <tr>
                <th><i class="far fa-user" aria-hidden="true"></i> S.No. </th>
                <th><i class="far fa-clock"></i> Date Time </th>
                <th><i class="far fa-user" aria-hidden="true"></i> Phone No. </th>
                <th><i class="fas fa-bars" aria-hidden="true"></i> Template Id </th>
                <th><i class="fas fa-th-large" aria-hidden="true"></i> Schedule</th>
                <th><i class="fas fa-th-large" aria-hidden="true"></i> Sent Status</th>
                <th><i class="fas fa-eye" aria-hidden="true"></i> Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
            <tr>
                <td>{{ $sr_no_start++ }}</td>
                <td>{{ $row->created_at }}</td>
                <td>{{ $row->phone_numbers }}</td>
                <td>{{ $row->template_id }}</td>
                <td>{{ $row->schedule_time }}</td>
                <td>
                @if($row->is_sent)
                  <span class="text-success">Sent</span>
                @else
                - -
                @endif
                </td>
                <td>
                  <a href="javascript:void(0)" class="edit" data-toggle="modal" data-target="#schedule_sms_{{$row->id}}"><span class="right-pad"><i class="fa fa-clock"></i> Re-Schedule</span></a>
                  <a href="javascript:void(0)" class="edit delete" data-href="{{ url('reports/cancel/'.$row->id) }}" data-toggle="modal" data-target="#deleteMessage"><span class="right-pad"><i class="fa fa-ban"></i> Cancel</span></a>
                  <a href="javascript:void(0)" class="delete" data-href="{{ url('reports/delete/'.$row->id) }}" data-toggle="modal" data-target="#deleteMessage"><span class="left-pad"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete</span></a>
                  {{ Form::open(array('url' => 'reports/re_schedule', 'class' => 'section-text re_schedule_form')) }}
                  {{Form::hidden('report_id',$row->id) }}
                  <?php $schedule_arr = explode(' ', $row->schedule_time);?>
                   <section class="insert-variable-modal">
                      <div class="modal" id="schedule_sms_{{$row->id}}">
                          <div class="modal-dialog">
                            <div class="modal-content"> 
                              <div class="modal-header">
                                <h4 class="modal-title section-text"><i class="fas fa-table" aria-hidden="true"></i> Schedule Date & Time</h4>
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>
                              <div class="modal-body section-text">                            
                                  <div class="form-row">
                                    <div class="form-group col-md-6 section-popup">
                                       <label for="title">Pick Date</label>
                                      {{Form::date('schedule_time',$schedule_arr[0],array('class'=>'form-control','id'=>'schedule_time3','required'=>true)) }}
                                    </div>
                                    <div class="form-group col-md-6 section-popup">
                                       <label for="title">Pick Time</label>
                                      {{Form::time('schedule_time2',$schedule_arr[1],array('class'=>'form-control','id'=>'schedule_time2','required'=>true)) }}
                                    </div>
                                  </div>
                              </div>            
                              <div class="modal-footer link">
                                <button type="submit" class="btn btn-blue-fill">Schedule Send <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
                                <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Close</button>
                              </div>
                            </div>
                          </div>
                      </div>
                  </section>
                  </form> 
                </td>
             </tr>
            @endforeach 
            </tbody>
          </table>
        </div>
        </div>
      </section>
      <section>
        <div class="row justify-content-end reoprts">
          <div class="col-md pagination_row">
              <div class="mt-select select-ct-4">
            {{ Form::select('pagination',get_pagination(),$current_page,array('class'=>'custom-select','id'=>'showItems')) }}
          </div>
          
            <div class="mt-pagination">
              {{ $data->links() }}
            </div>
          </div>
        </div>
      </section>
      @else
      <span class="text-danger">No Results Found!! <a href="{{ url('reports/scheduled_reports') }}" class="btn-link btn-blue-fill">Back</a></span>
      @endif
    </div>
  </section>
</div>

<section class="link-modal">
    <div class="modal fade" id="deleteMessage" tabindex="-1" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><i class="far fa-trash-alt" aria-hidden="true"></i>Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true"><i class="fas fa-times"></i></span>
            </button>
          </div>
          <div class="modal-body">
            <section>
              <div class="form-row">
                <div class="form-group col-sm-12">
                  <label for="variableName">Do you want to do this?</label>
                </div>
              </div>
            </section>
          </div>
          <div class="modal-footer">
            <div class="right-pad d-inline">
              <a href="" class="btn btn-blue-fill" id="delConfirm"><i class="far fa-check-circle"></i>Confirm</a>
            </div>
            <div class="left-pad d-inline">
              <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
            </div>

          </div>
        </div>
      </div>
    </div>
</section>  
@endsection