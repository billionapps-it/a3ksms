@extends('admin.layouts.default')
@section('pageTitle', 'Delivery Reports')
@section('content')
<div class="container ct-pad">
  <section class="mt template">
    <div class="head">
      <h3><img src="{{ asset('public/images/Icons/Manage Templates/Manage-template.svg') }}"> Delivery Reports</h3>
    </div>
    <div class="body-container"> 
      <section>
        {{ Form::open(array('url' => 'reports/delivery_reports', 'method'=>'get', 'class' => 'section-text','id' => 'export_form')) }}
          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="from_date">From Date</label>
              {{Form::text('from_date',$from_date,array('class'=>'form-control datepickeronly','id'=>'from_date','data-date-format'=>'yyyy-mm-dd', 'autocomplete'=>'off')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="to_date">To Date</label>                   
              {{Form::text('to_date',$to_date,array('class'=>'form-control datepickeronly','id'=>'to_date','data-date-format'=>'yyyy-mm-dd','autocomplete'=>'off')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="app_id">App Type</label>                   
              {{Form::select('app_id',$app_types,$app_id,array('class'=>'custom-select','id'=>'app_id')) }}
            </div>
            <div class="form-group col-md-2">
              <label for="message_source">Message Source</label>                   
              {{Form::select('message_source',$message_sources,$selected_source,array('class'=>'custom-select','id'=>'message_source')) }}
            </div>
            <div class="form-group col-md-4 text-right reports_btns">                  
              <button type="submit" class="btn btn-link btn-blue-fill" style="margin-top: 22px;">Search</button>
                <div class="d-inline">       
            {{ Form::close() }} 
        {{ Form::open(array('url' => 'reports/export','method'=>'POST','id'=>'export_for_reports')) }}
          <a href="javascript:void(0)" class="btn btn-link btn-blue-fill mr-none" id="exportExcelReports" style="margin-top: 22px;"><i class="fa fa-file-excel-o"></i> Export</a>
          {{ Form::hidden('exp_from_date',null,array('id'=>'exp_from_date')) }}
          {{ Form::hidden('exp_to_date',null,array('id'=>'exp_to_date')) }}
          {{ Form::hidden('exp_app_id',null,array('id'=>'exp_app_id')) }}
          {{ Form::hidden('exp_message_source',null,array('id'=>'exp_message_source')) }}
        {{ Form::close() }}
        </div>
            </div>
          </div> 
      </section>
       @if(count($data)>0)
      <section>
        <div class="form-group pad-bottom col-sm-12">
          <div class="mt-list">
          <table class="table table-striped table-bordered table-hover text-center">
            <thead class="thead-dark">
              <tr>
                <th><i class="far fa-user" aria-hidden="true"></i> S.No. </th>
                <th><i class="far fa-clock"></i> Date Time </th>
                <th><i class="far fa-user" aria-hidden="true"></i> Phone No. </th>
                <th><i class="fas fa-bars" aria-hidden="true"></i> Template Id </th>
                <th><i class="fas fa-th-large" aria-hidden="true"></i> Schedule</th>
                <th><i class="fas fa-th-large" aria-hidden="true"></i> Sent Status</th>
                <th><i class="fas fa-th-large" aria-hidden="true"></i> Response</th>
              </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
            <tr>
                <td>{{ $sr_no_start++ }}</td>
                <td>{{ $row->created_at }}</td>
                <td>{{ $row->phone_numbers }}</td>
                <td>{{ $row->template_id }}</td>
                <td>
                <?php 
                if($row->send_type == 'scheduled'){
                  echo $row->schedule_time;
                }else{
                  echo 'One Time';
                }
                ?>
                </td>
                <td>
                @if($row->is_sent)
                  <span class="text-success">Sent</span>
                @else
                - -
                @endif
                </td>
                <td>{{ $row->response }}</td>
             </tr>
            @endforeach 
            </tbody>
          </table>
        </div>
        </div>
      </section>
      <section>
        <div class="row justify-content-end reports">
          <div class="col-md pagination_row">
          <div class="mt-select select-ct-4">
            {{ Form::select('pagination',get_pagination(),$current_page,array('class'=>'custom-select','id'=>'showItems')) }}
              </div>
            <div class="mt-pagination">
              {{ $data->links() }}
            </div>
          </div>
          
        </div>
      </section>
      @else
      <span class="text-danger">No Results Found!! <a href="{{ url('reports/delivery_reports') }}" class="btn-link btn-blue-fill">Back</a></span>
      @endif
    </div>
  </section>
</div>
@endsection