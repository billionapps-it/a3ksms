<table>
  <thead>
    <tr>
      <th>Date Time </th>
      <th>Phone No. </th>
      <th>Template Id </th>
      <th>Schedule</th>
      <th>Sent Status</th>
      <th>Response</th>
      <th>Sent By</th>
      <th>App ID</th>
      <th>SID</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data as $row)
  <tr>
      <td>{{ $row->created_at }}</td>
      <td>{{ $row->phone_numbers }}</td>
      <td>{{ $row->template_id }}</td>
      <td>
      @if($row->send_type == 'scheduled')
        {{ $row->schedule_time }}
      @else
        {{ 'One Time' }}
      @endif
      </td>
      <td>
      @if($row->is_sent)
        <span class="text-success">Sent</span>
      @else
      - -
      @endif
      </td>
      <td>{{ $row->response }}</td>
      <td>{{ $row->created_by }}</td>
      <td>{{ $row->app_id }}</td>
      <td>{{ $row->sid }}</td>
   </tr>
  @endforeach 
  </tbody>
</table>