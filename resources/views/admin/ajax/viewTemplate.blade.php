<style>
.click-to-expand-wrapper.is-snippet-wrapper {
  transform: none;
  margin: 0;
  border: none;
  padding: 1em;
  line-height: 1.5;
  overflow-y: hidden;
    
}
/**recode**/ 
.click-to-expand-wrapper.is-expandable {
    cursor: pointer;
    overflow: hidden;
}
.example-request__selector, .example-request__status, .example-response__selector, .example-response__status {
    text-align: right;
    padding-right: 16px;
    margin-top: -21px;
}
.request-url {
    word-break: break-all;
    border: 1px solid #e6e6e6;
    padding: 6px 10px;
    border-radius: 3px;
    font-size: 12px;
    color: #282828;
    background-color: #f8f8f8;
}
.info .api-information pre {
    padding: 5px 10px;
    background: #fafafa;

}
.info .api-information code, .info .api-information pre {
    color: grey;
    border-radius: 3px;
    border: thin solid #e6e6e6;
    font-family: Courier,sans-serif;
}
.example-request__header, .example-response__header {
    margin: 0px 0;
    position: relative;
    background-color: #313131;
}
.click-to-expand-wrapper.is-expandable {
    cursor: pointer;
    overflow: hidden;
    background-color: black;
}
.curl_0_sendSMSf {
    color: greenyellow;
    margin-left: -43%;
}
span.token.operator {
    margin-left: -50%;
}
span.token.string {
    color: greenyellow;
    margin-left: -50%;
}
pre.request-urls {
    word-break: break-all;
    border: 1px solid #e6e6e6;
    padding: 6px 10px;
    border-radius: 3px;
    font-size: 12px;
    color: #282828;
    background-color: #f8f8f8;
}
span.token {
    margin-left: -49%;
}
.dropdown-toggle {
    white-space: nowrap;
    color: white;
}
.example-request__title {
    color: gray;
}
div#sendSMSf {
    margin-right: 0px;
    margin-left: 0px;
}
</style>

	<div class="row row-no-padding row-eq-height collection-item" id="sendSMSf">
    <div class="col-md-6 col-xs-12 section pd-15">
  			<div class="api-information">
    			<div class="heading">
  					<div class="name">
    					<div class="request-method-size-lg request-method-inline">
    						<div class="request-method--POST request-method" title="POST">
    							
    							<hr>

    							<span style="color: orange">POST</span> sendSMS</span>
    						</div>
    					</div>
  					</div>
				</div>
				<br>
				<div class="request-url-container">
					<div class="request-url">{{ url('api/v1/sendSMS')}}</div>
				</div>
				
    			<br><br>
      			<div class="headers">
    				<div class="heading">HEADERS</div>
    					<hr>
    						<div class="headers-key-value-table">
    							<div class="table-component">
  									<div class="table-component--table">

    								<div class="table-body">
      									<div class="table-row param row">
          									<div class="table-cell col-md-4 col-xs-12 name"><b>Authorization-token</b>
          									</div>
          								<div class="table-cell col-md-8 col-xs-12 value">
              								<div class="value col-xs-12">3MPHJP0BC63435345341</div>
          								</div>
     								 </div>
     								  
      								<div class="table-row param row">
      									<br>
          								<div class="table-cell col-md-4 col-xs-12 name"><b>Accept</b>
          								</div>
          							<div class="table-cell col-md-8 col-xs-12 value">
              							<div class="value col-xs-12">application/json</div>
          							</div>
      							</div>
    						</div>
  						</div>
					</div>
				</div>

			</div>
	  <div class="api-information">
    			<div class="heading">
  					<div class="name">
    					<div class="request-method-size-lg request-method-inline">
    						<div class="request-method--POST request-method" title="POST">
    							<br>
    							<br>
    							<span>BODY Raw</span>
    							<hr>
    						</div>
    					</div>
  					</div>
				</div>
				<br>
				
    			<div class="headers">
    				<div class="example-request__snippet">
	    			<div class="request code-snippet">
	      				<div class="formatted-requests is-default" data-lang="curl" data-id="sendSMSf_0">
	        				<pre class="request-urls">
	        					<code class=" is-highlighted language-javascript" id="curl_0_sendSMSfs">
<span class="token"></span>{ 
	<span class="token operator"></span>"template_id": "{{ $template->template_id }}", 
	<span class="token operator"></span>"app_id": "{{ $template->choose_app }}", 
	<span class="token operator"></span>"message": "{{ $template->message }}", 
	<span class="token"></span>"schedule_datetime": "", 
	<span class="">"recipient_number": [<span> 
	<span class="token operator"></span>"8887779990" 
	<span class="token operator">],</span> 
	<span class="token operator">"dynamic_value": [{<span> 
		<span class="token operator"></span>"Key":"Value", 
		<span class="token operator"></span>"Key":"Value", 
	<span class="token">}],</span> 
<span class="token">},</span> 
									
								</code>
							</pre>
						</div>
					</div>
				</div>
  			</div>
		  </div>
	


	</div>
</div>
<div class="col-md-6 col-xs-12 examples pd-15">
	
    <div class="example-request">
  		<div class="example-request__header">
  			
    		<div class="example-request__title">Example Response</div>
    			<div class="example-request__selector">
      			<div class="dropdown example-name example-name-dropdown">
  					<div class="dropdown-toggle truncate" id="" title="sendSMS_API"  >
							<span class="example-name__label">sendSMS</span>
    							 
  					</div>
  					</div>
    			</div>
  			</div>	
  			<div class="example-request__snippet">
    			<div class="request code-snippet">
      				<div class="formatted-requests is-default" data-lang="curl" data-id="sendSMSf_0" >
        				<pre class="click-to-expand-wrapper is-snippet-wrapper is-expandable  language-javascript" data-title="Example Request" data-id="sendSMSf_0" data-clipboard-target="#curl_0_sendSMSf" data-before-copy="Copy to Clipboard" data-after-copy="Copied">
<p>POST /api/v1/sendSMS HTTP/1.1</p>
<code class=" is-highlighted language-javascript" id="curl_0_sendSMSf">
curl <span class="token operator">--</span>location <span class="token operator">--</span>request POST <span class="token string">'{{ url('api/v1/sendSMS') }}'</span> \
<span class="token operator">--</span>header <span class="token string">'Authorization-token: 3MPHJP0BC63435345341'</span> \
<span class="token operator">--</span>header 
<span class="token string">'Accept: application/json'</span> \
<span class="token operator">--</span>data
								
</code>
						</pre>
					</div>
				</div>
			</div>
		</div>
	

	<br>

    <div class="example-request">
  		<div class="example-request__header">
  			
    		<div class="example-request__title">Example Response</div>
    			<div class="example-request__selector">
        			<div class="dropdown example-name example-name-dropdown">
    					<div class="dropdown-toggle truncate" id="sendSMSf_dropdown" title="sendSMS_API" data-toggle="dropdown">
      							<span class="example-name__label">200 OK</span>
      							<span class="caret"></span>
    					</div>
  					</div> 
    			</div>
  			</div>	
  			<div class="example-request__snippet">
    			<div class="request code-snippet">
      				<div class="formatted-requests is-default" data-lang="curl" data-id="sendSMSf_0">
        				<pre class="click-to-expand-wrapper is-snippet-wrapper is-expandable  language-javascript" data-title="Example Request" data-id="sendSMSf_0" data-clipboard-target="#curl_0_sendSMSf" data-before-copy="Copy to Clipboard" data-after-copy="Copied">
<code class="is-highlighted language-javascript" id="curl_0_sendSMSf">{ 
<span class="token operator">"success"</span>: true, 
<span class="token operator">"message"</span>[
	<span class="token operator">{</span>
 	<span class="token string">"success"</span>: false
  	<span class="token operator">"message"</span>"[HTTP 400] Unable to create record: The 'To' number 8887779990 is not a valid phone number." 
  	<span class="token string">"sid"</span>""
  	<span class="token operator">}</span> 
<span class="token operator">]</span> 
}
  								
  							</code>
  						</pre>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
</div>