<div class="modal-header">
   <h4 class="modal-title section-text"><i class="fas fa-eye" aria-hidden="true"></i> Preview SMS</h4>
   <button type="button" class="close" data-dismiss="modal"><i class="far fa-times-circle"></i></button>
 </div>
 <div class="modal-body section-text">                            
      <div class="form-row text-pad-bot">
        <div class="form-group col-md-12 section-popup">
           <h5><label for="title">Total processed rows: {{ $processed_row }}</label></h5> 
        </div>
        <div class="form-group col-md-12 section-popup">
           <h5><label for="title" class="title-green"><i class="fas fa-check" aria-hidden="true"></i> Valid: {{ $valid_row }} Rows</label> </h5>
        </div>
         @if($error_detail !='')
         <div class="form-group col-md-12 section-popup">
            <h5><label for="title" class="title-danger"><i class="fas fa-times" aria-hidden="true"></i> Error: {{ $invalid_row }} Rows</label> </h5>
         </div>         
         @endif
      </div>
      @if($error_detail !='')
      <div class="form-row scroll-width">
         <div class="form-group col-md-12 section-popup-error">
             <h5 class="header-popup"><i class="fas fa-exclamation-circle" aria-hidden="true"></i> Error Details</h5> 
             <hr/>
             <div class="content-scroll"> 
               <?php echo $error_detail;?>
              </div>
         </div>
     </div>
     @endif
      @if($send_type == 'scheduled')
      <div class="form-row">
        <div class="form-group col-md-6 section-popup">
           <label for="title">Pick Date</label>
          {{Form::date('schedule_time3',null,array('class'=>'form-control','id'=>'schedule_time3')) }}
        </div>
        <div class="form-group col-md-6 section-popup">
           <label for="title">Pick Time</label>
          {{Form::time('schedule_time4',null,array('class'=>'form-control','id'=>'schedule_time4')) }}
        </div>
      </div>
      @endif
 </div>    
 <hr/>        
 <div class="modal-footer link buttons-right1">
   <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="fa fa-times-circle"></i> Cancel</button>
   @if($error_detail !='')
   <a href="{{ url('public/download/error_log.txt') }}" class="btn btn-orange-outline" id="download_error_logs" download><i class="fa fa-download"></i> Download log</a>
   @endif
  @if($send_type == 'scheduled')  
   <button type="submit" class="btn btn-blue-fill send_type" data-type="scheduled"><i class="fa fa-check"></i> Schedule it <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
  @else
    <button type="submit" class="btn btn-blue-fill send_type" data-type="one_time"><i class="fa fa-check"></i> Send it <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
  @endif 
 </div>