@if(count($variables)>0)
<div class="form-row">
   <div class="col-md-9">
      <div class="form-group row">
         <label for="Match" class="col-sm-2 col-form-label">Match the Template variable with CSV Values*</label>
         <div class="col-sm-10 form-textbox-padding">
            <label>  Template: {{ $template->title }} </label>
         </div>
      </div>
   </div>
</div>
<div class="form-row form-textbox-padding">
   <div class="col-md-9">
      <div class="form-group row form-text-padding">
         <div class="offset-sm-2 col-sm-3">
            <label>  Template Variables </label>
         </div>
         <div class="col-sm-3">
            <label> CSV Values </label>
         </div>
      </div>
      <?php $i = 0;
      foreach($variables as $key=>$val){        
         $val = str_replace('##', '', $val);
         $possible_values = array();
         $possible = \App\InsertVariable::whereRaw("FIND_IN_SET('".$val."',possible_values)")
                     ->OrWhere('name',$val)->first();
         if(!empty($possible)){
            $possible_values = explode(',',$possible->possible_values);
            $possible_values = array_map('trim', $possible_values);
            //dd($possible_values);
         }         
         ?>
      <div class="form-group row form-space">
         <div class="offset-sm-2 col-sm-3 radio-pad-top">
            {{Form::radio("",null, true, array('class'=>'radio','style="cursor: unset"')) }}
            {{Form::checkbox("dynamic[$i][template_variables]",$val, true, array('class'=>'radio dynamic_checkbox','style="display:none"')) }} {{ $val }}
         </div>
         <div class="col-sm-3">
            <div class="select-ct-1">
               <select class="custom-select csv_headers" name="dynamic[{{$i}}][csv_headers]">
                  <option value="">Select</option>
                  @foreach($csv_headers as $header)
                     <?php 
                        $selected = '';
                        if(in_array($header, $possible_values)){
                           $selected = ' selected';
                        }
                     ?>
                     <option value="{{ $header }}" {{ $selected }}>{{ $header }}</option>
                  @endforeach
                     <option value="custom_value">Custom Value</option>
               </select>
            </div>
         </div>
         <div class="col-sm-4 textbox-padleft">
            <input class="form-control custom_val" name="dynamic[{{$i}}][custom_val]" type="text">
         </div>
      </div>
      <?php 
      $i++;
      }?>
      <div class="form-group row form-space">
         <div class="offset-sm-2 col-sm-3 radio-pad-top">
            Phone Number Mapping
         </div>
         <div class="col-sm-3">
            <div class="select-ct-1">
               <select class="custom-select" name="csv_phone_number">
                  @foreach($csv_headers as $header)
                     <?php 
                        $selected = '';
                        if($header == 'response'){
                           $selected = ' selected';
                        }
                     ?>
                     <option value="{{ $header }}" {{ $selected }}>{{ $header }}</option>
                  @endforeach
               </select>
            </div>
         </div>
         <div class="col-sm-4 textbox-padleft">
            
         </div>
      </div>
   </div>
</div>
@endif
                         