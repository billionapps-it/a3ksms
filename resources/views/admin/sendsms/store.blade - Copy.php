@extends('admin.layouts.default')
@section('pageTitle', 'Send SMS')
@section('content')
<div class="container ct-pad">
    <section class="template send_message">
       <div class="head">
          <h3><i class="far fa-list-alt"></i> Send Message</h3>
       </div>
       <div class="body-container">
          <section>
              {{ Form::open(array('url' => 'sms/sendmessage', 'files' => true, 'class' => 'section-text','id'=>'sendSMSForm')) }}
                <div class="form-row">
                   <div class="form-group col-md-10">
                      <div class="form-group row">
                         <label for="phone_number" class="col-sm-2 col-form-label">Phone Number</label>
                         <div class="col-sm-5">
                          {{Form::text('phone_number',null,array('class'=>'form-control','id'=>'phone_number' )) }}
                          <p class="input_label">add multiple numbers separated by commas for ex9999988888, 8888899999</p>
                         </div>
                         <label class="col-sm-1 or_message col-form-label"> or </label>
                         <label for="myInput" class="col-sm-2 col-form-label" style="display: none">Upload your CSV 
                          <div>
                            <a href="{{ url('public/download/phones_sample.csv') }}" class="download_sample">Download Sample</a></div>
                          </label>
                         <div class="col-sm-2 file_upload">
                            <input id="uploadCSV" name="uploadCSV" type="file" style="visibility:hidden"/>
                            <span title="Upload File" class="attachFileSpan" onclick="document.getElementById('uploadCSV').click()"><i class="fa fa-upload" aria-hidden="true"></i>
                            Upload your CSV 
                            </span>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="form-row">
                   <div class="form-group col-md-10">
                      <div class="form-group row">
                         <label for="template_id" class="col-sm-2 col-form-label">Choose a Template</label>
                         <div class="col-sm-5">
                            <div class="select-ct-1">
                              {{Form::select('template_id',$templates,null,array('class'=>'custom-select','id'=>'template_id' )) }}
                            </div>                         
                          </div>
                        </div>
                    </div>
                </div>
                <!--static Start--->
              <div id="dynamic_value_here"></div>
                <!--static End--->
              <div class="form-row">
                  <div class="form-group col-md-10">
                      <div class="form-group row">
                         <label for="message" class="col-sm-2 col-form-label">Message to Send</label>
                         <div class="col-sm-10">
                          {{Form::textarea('message',null,array('class'=>'form-control messageCountType','rows'=>10,'id'=>'message' )) }}
                      </div>
                   </div>
                </div>
              </div>

            <div class="modal" id="send_sms">
                <div class="modal-dialog">
                  <div class="modal-content" id="send_sms_data"> 
                    
                  </div>
                </div>
            </div>
            
              <div class="form-row">
                 <div class="form-group col-md-10">
                    <div class="form-group row">
                       <label for="inputPassword" class="col-sm-2 col-form-label hide_mob"></label>
                        <div class="col-sm-8" id="send_sms_without_csv">
                         <div class="right-pad d-inline">
                            <button type="submit" class="btn btn-link btn-blue-fill">Send SMS <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
                         </div>
                         <div class="left-pad d-inline">
                            <button type="button" class="btn btn-link btn-blue-border" data-toggle="modal" data-target="#schedule_sms">Schedule Send</button>
                         </div>
                        </div>
                        <div class="col-sm-8" id="send_sms_with_csv">
                         <div class="right-pad d-inline">
                            <button type="button" data-type="imediate_send" class="btn btn-link btn-blue-fill send_sms_with_csv_btn">Send SMS <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
                         </div>
                         <div class="left-pad d-inline">
                            <button type="button" data-type="schedule_send" class="btn btn-link btn-blue-border send_sms_with_csv_btn">Schedule Send</button>
                         </div>
                        </div>
                    </div>
                 </div>
              </div>

              <section class="insert-variable-modal">

                  <div class="modal" id="schedule_sms">
                      <div class="modal-dialog">
                        <div class="modal-content"> 
                          <div class="modal-header">
                            <h4 class="modal-title section-text"><i class="fas fa-table" aria-hidden="true"></i> Schedule Date & Time</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>
                          <div class="modal-body section-text">                            
                              <div class="form-row">
                                <div class="form-group col-md-6 section-popup">
                                   <label for="title">Pick Date</label>
                                  {{Form::date('schedule_time3',null,array('class'=>'form-control','id'=>'schedule_time3')) }}
                                </div>
                                <div class="form-group col-md-6 section-popup">
                                   <label for="title">Pick Time</label>
                                  {{Form::time('schedule_time4',null,array('class'=>'form-control','id'=>'schedule_time4')) }}
                                </div>
                              </div>
                          </div>            
                          <div class="modal-footer link">
                            <button type="submit" class="btn btn-blue-fill">Schedule Send <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>     
                            <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Close</button>
                          </div>
                        </div>
                      </div>
                  </div>
              </section>

             {{ Form::close() }}
          </section>
       </div>
    </section>
 </div>

<!--message popup start-->
<section class="insert-variable-modal">

  <div class="modal" id="missing_template_popup">
      <div class="modal-dialog">
        <div class="modal-content"> 
          <div class="modal-header">
            <h4 class="modal-title section-text"><i class="fa fa-file-excel-o"></i> Missing Template Variables Mapping</h4>
            <button type="button" class="close" data-dismiss="modal"><i class="far fa-times-circle"></i></button>
          </div>
          <div class="modal-body section-text">                            
              <div class="form-row">
                <div class="form-group col-md-12 section-popup">
                  Please check below the Template Variables and need to be mapped with appropriate CSV value, do the currection and re-submit it.
                 </div>
                 <ul id="missing_template_variables">
                   
                 </ul>
              </div>
          </div>    
          <hr/>        
          <div class="modal-footer link buttons-right">
            <button type="button" class="btn btn-blue-fill" data-dismiss="modal"><i class="fa fa-check"></i> OK</button>
          </div>
        </div>
      </div>
  </div>
  
</section>
<!-- message popup end-->
@endsection