@extends('admin.layouts.default')
@section('pageTitle', 'Create Message Type')
@section('content')
<div class="container ct-pad">
    {{ Form::open(array('url' => 'settings/createmessagetype', 'files' => true, 'class' => 'section-text','id'=>'form')) }}
    <section class="template">
      <div class="head">
        <h3><i class="far fa-list-alt" aria-hidden="true"></i> Create Message Type</h3>
      </div>
      <div class="body-container">
        <section>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="name">Message Type Name*</label>
                {{Form::text('name',null,array('class'=>'form-control','id'=>'name')) }}
              </div>
            </div>           
        </section>
      </div>
    </section>
    <section>
      <div class="row">
        <div class="col-sm-12">
          <div class="right-pad d-inline">
            <a href="{{ url('settings/messagetypesetting') }}" class="btn btn-link btn-red-outline"><i class="far fa-times-circle"></i>Cancel</a>
          </div>
          <div class="left-pad d-inline">
            <button type="submit" class="btn btn-link btn-blue-fill">Create <i class="fa fa-spinner" style="display: none;"></i></button>
          </div>
        </div>
      </div>
    </section>
    {{ Form::close() }}
  </div>
@endsection