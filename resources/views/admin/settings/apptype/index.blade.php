@extends('admin.layouts.default')
@section('pageTitle', 'App Setting')
@section('content')
<div class="container ct-pad">
  <section class="mt template">
    <div class="head">
      <h3><img src="{{ asset('public/images/Icons/Manage Templates/Manage-template.svg') }}"> App Setting</h3>
    </div>
    <div class="body-container">
      <section>
        <div class="form-group row pad-bottom">
          <div class="col-sm-4">
            <div class="right-pad d-inline">
              
            </div>
          </div>
          <div class="col-sm-6 text-sm-right">
            <div class="right-pad d-inline">
              
            </div>
            <div class="left-pad d-inline">
              <a href="{{ url('settings/createapp') }}" class="btn btn-link btn-blue-outline"><i class="far fa-list-alt"></i>Create App</a>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div class="form-group pad-bottom col-sm-10">
          <div class="mt-list">
          <table class="table table-striped table-bordered table-hover text-center">
            <thead class="thead-dark">
              <tr>
                <th><i class="far fa-user"></i> S.No. </th>
                <th><i class="far fa-user"></i> App ID </th>
                <th><i class="fas fa-bars"></i> App Name </th>
                <th><i class="fas fa-th-large"></i> Auth Key</th>
                <th class="border-radius-tr"><i class="far fa-eye" aria-hidden="true"></i> Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach($data as $row)
               <tr>
                  <td>{{ $sr_no_start++ }}</td>
                  <td>{{ $row->id }}</td>
                  <td>{{ $row->name }}</td>
                  <td>{{ $row->auth_key }}</td>
                  <td>
                      <span class="right-pad"><a href="{{ url('settings/editapp/'.$row->id) }}" class="edit" ><i class="far fa-edit"></i> Edit</a></span>
                      <span class="left-pad"><a href="javascript:void(0)" class="delete" data-href="{{ url('settings/deleteapp/'.$row->id) }}" data-toggle="modal" data-target="#deleteModal"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete</a></span>
                    </td>
                  </tr>
                  @endforeach
            </tbody>
          </table>
        </div>
        </div>
      </section>
      <section>
        <div class="row justify-content-end col-sm-10 no-pagination">
          <div class="col-md pagination_row">
          <div class="mt-select select-ct-4">
            {{ Form::select('pagination',get_pagination(),$current_page,array('class'=>'custom-select','id'=>'showItems')) }}
            </div>
            <div class="mt-pagination">
              {{ $data->links() }}
            </div>
          </div>
          
        </div>
      </section>
    </div>
  </section>
</div>

<section class="link-modal">
      <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
              </button>
            </div>
            <div class="modal-body">
              <section>
                <div class="form-row">
                  <div class="form-group col-sm-12">
                    <label>Do you want to delete?</label>
                  </div>
                </div>
              </section>
            </div>
            <div class="modal-footer">
              <div class="right-pad d-inline">
                <a href="" class="btn btn-blue-fill" id="delConfirm"><i class="far fa-check-circle"></i>Confirm</a>
              </div>
              <div class="left-pad d-inline">
                <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
              </div>

            </div>
          </div>
        </div>
      </div>
  </section>  
@endsection