@extends('admin.layouts.default')
@section('pageTitle', 'Create App')
@section('content')

<div class="container ct-pad">
  {{ Form::open(array('url' => 'settings/createapp', 'files' => true, 'class' => 'section-text','id'=>'form')) }}
  <section class="template">
    <div class="head">
      <h3><i class="far fa-list-alt" aria-hidden="true"></i> Create App</h3>
    </div>
    <div class="body-container">
      <section>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name">App Name</label>
              {{Form::text('name',null,array('class'=>'form-control','id'=>'name')) }}
            </div>
            <div class="form-group col-md-6">
              <label for="app">Auth Key</label>                   
              {{Form::text('auth_key',$random_auth_key,array('class'=>'form-control box' )) }}
             </div>
          </div>           
      </section>
    </div>
  </section>
  <section>
    <div class="row">
      <div class="col-sm-12">
        <div class="right-pad d-inline">
          <a href="{{ url('settings/appsetting') }}" class="btn btn-link btn-red-outline"><i class="far fa-times-circle"></i>Cancel</a>
        </div>
        <div class="left-pad d-inline">
          <button type="submit" class="btn btn-link btn-blue-fill">Create <i class="fa fa-spinner" style="display: none;"></i></button>
        </div>
      </div>
    </div>
  </section>
  {{ Form::close() }}
</div>
@endsection