@extends('admin.layouts.default')
@section('pageTitle', 'Edit Template')
@section('content')
<div class="container ct-pad">
  {{ Form::open(array('url' => 'template/edittemplate/'.$template->id, 'files' => true, 'class' => 'section-text','id'=>'form')) }}
    <section class="template">
      <div class="head">
        <h3><i class="far fa-list-alt"></i> Edit Template</h3>
      </div>
      <div class="body-container">
        <section>
            <div class="form-row">
              <div class="form-group col-md-10">
                <label for="inputTitle">Title</label>
                {{Form::text('title',$template->title,array('class'=>'form-control box','maxlength'=>26 )) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5 right-pad">
                <label for="messageType">Message Type</label>
                <div class="select-ct-1">
                {{Form::select('message_type',$messageTypes,$template->message_type,array('class'=>'custom-select','id'=>'messageType' )) }} 
              </div>
              </div>
              <div class="form-group col-md-5 left-pad">
                <div class="select-ct-2">
                <label for="chooseTheApp">Choose the App</label>
                {{Form::select('choose_app',$appTypes,$template->choose_app,array('class'=>'custom-select' ))}}
                </div>
              </div>
            </div>
            <div class="form-group col-md-10">
              <label for="msgCount">Message</label>
              {{Form::textarea('message',$template->message,array('class'=>'form-control col-md-10','id'=>'msgCount','rows'=>10 )) }}
            </div>
        </section>
      </div>
    </section>
    <section>
      <div class="row">
        <div class="col-sm-5 exclamation-text">
          <i class="fa fa-exclamation-circle"></i> You have used <span id="charecterShow"> {{ strlen($template->message) }} </span> characters
          <!--button type="button" class="btn btn-link btn-blue-outline"><i class="fas fa-link"></i>Link</button-->
        </div>
        <div class="col-sm-5 text-sm-right">
          <a href="javascript:void(0)" class="btn btn-link btn-blue-fill" data-toggle="modal" data-target="#insertVariable" onclick="insertVariableForm.reset();"><i class="fas fa-border-all"></i> Insert Variable</a>
        </div>
        <div class="col-sm-12">
          <div class="right-pad d-inline">
            <a href="{{ url('templates') }}" class="btn btn-link btn-red-outline"><i class="far fa-times-circle"></i> Cancel</a>
          </div>
          <div class="left-pad d-inline">
            <button type="submit" class="btn btn-link btn-blue-fill"><i class="far fa-list-alt"></i> Update Template <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
          </div>
        </div>
      </div>
    </section>
  {{ Form::close() }}
  </div>
  <section class="insert-variable-modal">
      <div class="modal fade" id="insertVariable" tabindex="-1"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="fas fa-border-all"></i>Insert Variable</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
              </button>
            </div>            
            {{ Form::open(array('url' => '', 'class' => 'section-text','id'=>'insertVariableForm','name'=>'insertVariableForm')) }}
            <div class="modal-body">
              <section>
                <div class="form-row">
                  <div class="form-group col-sm-5">
                    <label for="chooseVariable">Choose the Variable from the list</label>
                    <div class="select-ct-3">
                    {{ Form::select('variabl_type',$insertVariables,null,array('class'=>'custom-select','id'=>'variabl_type' )) }}  
                  </div>
                  </div>
                  <div class="col-sm-1 d-flex justify-content-center d-flex align-items-center">
                    <p>Or</p>
                  </div>
                  <div class="form-group col-sm-5">
                    <label for="variableName">Create a Variable</label>
                    {{Form::text('variable_name',null,array('class'=>'form-control' ,'id'=>'variable_name')) }}
                    <div class="col-sm-12 text-right example-text">
                      <p>ex: FirstName</p>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-sm-12">
                    <label for="possible_values">CSV header mapping field</label>
                    <div class="col-sm-12 text-right example-text">
                      {{Form::text('possible_values',null,array('class'=>'form-control' ,'id'=>'possible_values')) }}
                      <p>ex: FirstName,First Name,first-name,first_name </p>
                    </div>
                  </div>
                </div>
              </section>
            </div>
            <div class="modal-footer">
              <div class="right-pad d-inline">
                <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Close</button>
              </div>
              <div class="left-pad d-inline">
                <button type="submit" class="btn btn-blue-fill">Insert <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>
              </div>
            </div>            
            {{ Form::close() }}
          </div>
        </div>
      </div>
  </section>

  <section class="insert-variable-modal">
    <div class="modal" id="otherpopupmodal">
        <div class="modal-dialog">
          <div class="modal-content">          
            {{ Form::open(array('url' => 'template/other/message_type', 'class' => 'section-text','id'=>'otherForm')) }}
            <div class="modal-header">
              <h4 class="modal-title section-text"><i class="fas fa-table" aria-hidden="true"></i> Other Message Type</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body section-text">
                <div class="form-row">
                  <div class="form-group col-md-12 section-popup">
                     <label for="title">Other Message Type</label>
                        {{Form::text('other',null,array('class'=>'form-control box' ,'id'=>'othervaribale')) }}
                  </div>
                </div>
            </div>            
            <div class="modal-footer link">
              <button type="submit" class="btn btn-blue-fill">Submit <i class="fa fa-spinner fa-spin" style="font-size: 19px; display: none;"></i></button>              
              <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Close</button>
            </div>
           {{ Form::close() }}
          </div>
        </div>
    </div>
  </section>

@endsection