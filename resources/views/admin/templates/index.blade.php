@extends('admin.layouts.default')
@section('pageTitle', 'Manage Templates')
@section('content')
 <div class="container ct-pad">
      <section class="mt template">
        <div class="head">
          <h3><img src="{{ asset('public/images/Icons/Manage Templates/Manage-template.svg') }}"> Manage Templates</h3>
        </div>
        <div class="body-container">
          <section>
            <div class="form-group row pad-bottom">
              <div class="col-sm-6">
                <div class="right-pad d-inline">
                  <a href="javascript:void(0)" class="btn btn-link btn-red-outline" id="bulk_delete_btn_template"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete Templates</a>
                </div>
              </div>
              <div class="col-sm-6 text-sm-right">
                <div class="right-pad d-inline">
                  <!--a href="javascript:void(0)" class="btn btn-link btn-orange-outline"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Template Guide</a-->
                </div>
                <div class="left-pad d-inline">
                  <a href="{{ url('template/createtemplate') }}" class="btn btn-link btn-blue-outline"><i class="far fa-list-alt"></i>Create Template</a>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="form-group pad-bottom col-sm-12">
              <div class="mt-list">
              <table class="table table-striped table-bordered table-hover text-center">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col"><input class="form-check-input" type="checkbox" id="selectall"></th>
                    <th><i class="far fa-user"></i> S.No. </th>
                    <th><i class="far fa-user"></i> Template ID </th>
                    <th><i class="fas fa-bars"></i> Template Name</th>
                    <th><i class="fas fa-th-large"></i> App</th>
                    <th><i class="fas fa-chart-line"></i> Status</th>
                    <th class="border-radius-tr"><i class="far fa-eye"></i> Action</th>
                  </tr>
                </thead>
                <tbody>
               @if($templates)
                @foreach($templates as $template)  
                  <tr>
                    <th scope="row"><input type="checkbox" class="form-check-input selectedId" aria-label="Checkbox for following text input" value="{{ $template->id }}"></th>
                    <td>{{ $sr_no_start++ }}</td>
                    <td>{{ $template->template_id}}</td>
                    <td>{{ $template->title }}</td>
                    <td>{{ $template->appType->name }}</td>
                    <td>
                      <?php if( $template->is_active =='1' ){ ?>
                      <button type="button" class="btn btn-green-fill">Active</button>
                    <?php }else{ ?>
                      <button type="button" class="btn btn-red">Inactive</button>
                    <?php } ?>
                    </td>
                    <td>
                      <a href="{{ url('template/edittemplate/'.$template->id) }}" class="edit right-pad"><i class="far fa-edit"></i> Edit</a>
                      <a href="javascript:void(0)" class="delete delthis right-pad" data-toggle="modal" data-target="#deleteTemplate"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete</a>
                      <a href="javascript:void(0)" class="left-pad1 api_preview_btn" data-id="{{ $template->id }}"><span class="success"><i class="far fa-eye" aria-hidden="true"></i>Preview</span></a>
                    </td>
                  </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
            </div>
          </section>
          <section>
            <div class="row justify-content-end manage_tmplate">
              <div class="col-md pagination_row">
              <div class="mt-select select-ct-4">
                {{ Form::select('pagination',get_pagination(),$current_page,array('class'=>'custom-select','id'=>'showItems')) }}
                    </div>
                <div class="mt-pagination">
                  {{ $templates->links() }}
                </div>
              </div>              
            </div>
          </section>
        </div>
      </section>
    </div>

  <section class="link-modal apipreview">
        <div class="modal fade" id="previewTemplateModal" tabindex="-1" aria-hidden="true">
          <div class="modal-dialog modal-lg custom-width">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title"><i class="far fa-question-circle" aria-hidden="true"></i>API Integration Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="fas fa-times"></i></span>
                </button>
              </div>
              <div class="modal-body" id="api_preview">
              </div>
              <div class="modal-footer">
                <div class="right-pad d-inline">
                  
                </div>
                <div class="left-pad d-inline">
                  <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
                </div>

              </div>
            </div>
          </div>
        </div>
    </section> 

  <section class="link-modal">
      <div class="modal fade" id="deleteTemplate" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="far fa-trash-alt" aria-hidden="true"></i>Delete Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
              </button>
            </div>
            <div class="modal-body">
              <section>
                <div class="form-row">
                  <div class="form-group col-sm-12">
                    <label for="variableName">Do you want to delete selected templates?</label>
                  </div>
                </div>
              </section>
            </div>
            <div class="modal-footer">
              <div class="right-pad d-inline">
                <button type="button" class="btn btn-blue-fill" data-href="{{ url('template/deletetemplateall') }}" id="bulk_delete"><i class="far fa-check-circle"></i>Confirm</button>
              </div>
              <div class="left-pad d-inline">
                <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
              </div>

            </div>
          </div>
        </div>
      </div>
  </section>  
  <section class="link-modal">
      <div class="modal fade" id="seelectWarning" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"><i class="far fa-question-circle" aria-hidden="true"></i>Select Warning</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="fas fa-times"></i></span>
              </button>
            </div>
            <div class="modal-body">
              <section>
                <div class="form-row">
                  <div class="form-group col-sm-12">
                    <label for="variableName">Please select at least one record to delete</label>
                  </div>
                </div>
              </section>
            </div>
            <div class="modal-footer">
              <div class="right-pad d-inline">
                
              </div>
              <div class="left-pad d-inline">
                <button type="button" class="btn btn-red-outline" data-dismiss="modal"><i class="far fa-times-circle"></i> Cancel</button>
              </div>

            </div>
          </div>
        </div>
      </div>
  </section> 
@endsection