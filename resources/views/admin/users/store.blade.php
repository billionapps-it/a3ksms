@extends('admin.layouts.default')
@section('pageTitle', 'Add User')
@section('content')
<div class="container ct-pad">
    {{ Form::open(array('url' => 'user/store', 'files' => true, 'class' => 'section-text','id'=>'form')) }}
    <section class="template">
      <div class="head">
        <h3><i class="fas fa-user-plus"></i>Create User</h3>
      </div>
      <div class="body-container">
        <section>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="userName">Username*</label>
                {{Form::text('first_name',null,array('class'=>'form-control','id'=>'userName','maxlength'=>26)) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="email">Email Address*</label>
                {{Form::email('email',null,array('class'=>'form-control email','id'=>'email' )) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="password">Default Password*</label>
                <input type="password" name="password" id="password" class="form-control noSpace" autocomplete="new-password" maxlength="12" minlength="6">
                <em class="text-danger">Enter min 6 and max 12 characters</em>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="phone">Phone*</label>
                {{Form::text('phone',null,array('class'=>'form-control phone','id'=>'phone' )) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5 right-pad">
                <label for="role_name">Choose User Type</label>
                <div class="select-ct-1">
                {{ Form::select('role_name',$roles,null,array('class'=>'custom-select')) }}
              </div>
              </div>
            </div>
            <div class="form-group">
              <label>Choose the App Access</label>
            </div>
            <div class="form-row user_app_access col-md-4">
                <div class="form-group">
                  <div class="form-check">
                    <input type="checkbox" name="app_access[]" class="" value="0" id="allAppAccessCheck"> 
                    <label class="form-check-label login-check" for="allAppAccessCheck">All</label>
                  </div>
                </div>
                @foreach($appTypes as $appType)
                <div class="form-group">
                  <div class="form-check">
                    {{ Form::checkbox('app_access[]',$appType->id,false, array('class'=>'app_access_check','id'=>"check_".$appType->id)) }}        
                    <label class="form-check-label login-check" for="check_{{ $appType->id}}"> {{ $appType->name }} </label>
                  </div>
                </div>
                @endforeach                
            </div>            
        </section>
      </div>
    </section>
    <section>
      <div class="row">
        <div class="col-sm-12">
          <div class="right-pad d-inline">
            <a href="{{ url('users') }}" class="btn btn-link btn-red-outline"><i class="far fa-times-circle"></i>Cancel</a>
          </div>
          <div class="left-pad d-inline">
            <button type="submit" class="btn btn-link btn-blue-fill"><i class="fas fa-envelope-open"></i>Create a user and send a email Notification <i class="fa fa-spinner" style="display: none;"></i></button>
          </div>
        </div>
      </div>
    </section>
    {{ Form::close() }}
  </div>
@endsection