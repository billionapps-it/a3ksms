@extends('admin.layouts.default')
@section('pageTitle', 'Edit User')
@section('content')
<div class="container ct-pad">
    {{ Form::open(array('url' => 'user/edit/'.$user->id, 'files' => true, 'class' => 'section-text','id'=>'form')) }}
    <section class="template">
      <div class="head">
        <h3><i class="fas fa-user-edit"></i>Edit User</h3>
      </div>
      <div class="body-container">
        <section>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="userName">Username*</label>
                {{Form::text('first_name',$user->first_name,array('class'=>'form-control','id'=>'userName','maxlength'=>26)) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="email">Email Address*</label>
                {{Form::email('email',$user->email,array('class'=>'form-control email','id'=>'email' )) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-10">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" class="form-control noSpace" autocomplete="new-password" maxlength="12" minlength="6">
                <em class="text-danger">Leave blank if you dont't want to change, Min 6 and max 12 characters</em>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-8">
                <label for="phone">Phone*</label>
                {{Form::text('phone',$user->phone,array('class'=>'form-control phone','id'=>'phone' )) }}
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-5 right-pad">
                <label for="role_name">Choose User Type</label>
                <div class="select-ct-1">
                {{ Form::select('role_name',$roles,$user->role_name,array('class'=>'custom-select')) }}
              </div>
              </div>
            </div>
            <div class="form-group">
              <label>Choose the App Access</label>
            </div>
            <div class="form-row user_app_access col-md-4">
                <div class="form-group">
                  <div class="form-check">
                  <?php 
                    $hasAccess = \App\AppTypeUser::where('app_type_id',0)->where('user_id',$user->id)->first();
                  ?>
                    <input type="checkbox" name="app_access[]" @if(isset($hasAccess)) checked @endif value="0" id="allAppAccessCheck"> 
                    <label class="form-check-label login-check" for="allAppAccessCheck">All</label>
                  </div>
                </div>
                @foreach($appTypes as $appType)
                <div class="form-group">
                  <div class="form-check">
                  <?php //dd($user->appAccesses);
                    $hasAccess = \App\AppTypeUser::where('app_type_id',$appType->id)->where('user_id',$user->id)->first();
                    $selected = isset($hasAccess)?true:false;
                  ?>
                    {{ Form::checkbox('app_access[]', $appType->id, $selected, array('class'=>'app_access_check','id'=>"check_".$appType->id)) }}        
                    <label class="form-check-label login-check" for="check_{{$appType->id}}"> {{ $appType->name }} </label>
                  </div>
                </div>
                @endforeach                
            </div>            
        </section>
      </div>
    </section>
    <section>
      <div class="row">
        <div class="col-sm-12">
          <div class="right-pad d-inline">
            <a href="{{ url('users') }}" class="btn btn-link btn-red-outline"><i class="far fa-times-circle"></i>Cancel</a>
          </div>
          <div class="left-pad d-inline">
            <button type="submit" class="btn btn-link btn-blue-fill"><i class="fas fa-user-cog"></i>Update User Settings<i class="fa fa-spinner" style="display: none;"></i></button>
          </div>
        </div>
      </div>
    </section>
    {{ Form::close() }}
  </div>
@endsection