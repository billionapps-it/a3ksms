<!doctype html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="shortcut icon" href="{{ asset('public/images/favicon.png') }}" />
      <title> @yield('pageTitle') | Achieve3000</title>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/brand.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/style.css') }}">
      <link rel="stylesheet" href="{{ asset('public/css/responsive.css') }}">
      <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />
      <link rel="stylesheet" type="text/css" href="{{ asset('public/backend/css/toastr.min.css') }}">
      <link href="https://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" rel="stylesheet">
      <script type="text/javascript">
         var site_url = "{{url('/')}}";
      </script>
   </head>
   <body>
      <!--- header start--->
      <?php $url = trim(preg_replace('/[0-9]+/', '', Request::path()),'/')?>
      <div class="container-fluid">
         <header class="head-section">
            <nav class="navbar navbar-expand-sm navbar-light">
               <div class="container">
                  <img src="{{ asset('public/images/logo.svg') }}" class="header-logo">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     <ul class="navbar-nav ml-auto">
                        <li class="nav-item  ml-3 @if($url == 'templates' || $url == 'template/createtemplate' || $url == 'template/edittemplate') active @endif">
                           <a class="nav-link" href="{{ url('templates') }}">Manage Templates <span class="sr-only">(current)</span></a>
                        </li>
                        @if(Auth::check() && Auth::user()->roles()->get()->first()->name == 'super_admin')
                        <li class="nav-item ml-3 @if($url == 'users' || $url == 'user/store' || $url == 'user/edit') active @endif">
                           <a class="nav-link" href="{{ url('users') }}">Manage Users</a>
                        </li>
                        @endif
                        <li class="nav-item ml-3 @if($url == 'sms/sendmessage') active @endif">
                           <a class="nav-link" href="{{ url('sms/sendmessage') }}">Send Message</a>
                        </li>
                        <li class="nav-item dropdown ml-3 @if($url == 'reports/delivery_reports') active @endif">
                           <a class="nav-link dropdown-toggle" href="{{ url('reports/delivery_reports') }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Reports
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{url('reports/delivery_reports')}}">Delivery Reports</a>
                              <a class="dropdown-item" href="{{ url('reports/scheduled_reports') }}">Schedule Reports</a>
                           </div>
                        </li>
                        <li class="nav-item dropdown ml-3 @if($url == 'settings/appsetting' || $url == 'settings/createapp' || $url == 'settings/editapp') active @endif">
                           <a class="nav-link dropdown-toggle" href="{{ url('settings/appsetting') }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Settings
                           </a>
                           <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{url('settings/appsetting')}}">Manage App</a>
                              <a class="dropdown-item" href="{{ url('settings/messagetypesetting') }}">Message Type</a>
                           </div>
                        </li>
                        <li class="nav-item logout">
                           <a class="btn btn-danger my-2 my-sm-0" href="{{ url('logout') }}">Logout</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </nav>
         </header>
         <!--- header end--->
         @if( session('success'))
         <div class="alert alert-success pt10 pb10 mt10 mb10">
            {{ session('success') }}
         </div>
         @endif
         @if( session('error'))
         <div class="alert alert-danger mt10 mb10  pt10 pb10">
            {{ session('error') }}
         </div>
         @endif
         @yield('content')
         <!--- footer section start--->
         <footer class="footer-bg">
            <div class="container">
               <div class="row">
                  <div class="col-md-3 footer-logo">
                     <img src="{{ asset('public/assets/img/logo copy 2.png') }}" class="">
                  </div>
                  <div class="col-md-9">
                     <div class="d-flex flex-row">
                     <div class="ml-auto">
                <a href="https://www.facebook.com/pages/Achieve3000/333675157246" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-facebook-f'></i></div></a>
                <a href="https://www.linkedin.com/company/achieve3000/" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-linkedin-in'></i></div></a>
                <a href="https://twitter.com/achieve3000" target="_blank" style="text-decoration:none;"><div class="icon d-inline-flex"><i class='fab fa-twitter'></i></div></a>
              </div>
                     </div>
                     <div class="d-flex justify-content-end">
                        <ul class="un-bull">
                           <li><a href="https://www.achieve3000.com/who-we-are/about/">About Us</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://achieve3000.applicantstack.com/x/openings">Careers</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/who-we-are/about/contact-us/">Contact Us</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/who-we-are/press-room/">Press Room</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/who-we-are/about/privacy-policy/">Privacy Policy</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/who-we-are/about/terms-use/">Terms of Use</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/who-we-are/about/terms-of-service/">Terms of Service</a></li>
                           <span class="vline">|</span> 
                           <li><a href="https://www.achieve3000.com/system-requirements-2020/">System Requirement</a></li>
                        </ul>
                     </div>
                     <div class="d-flex justify-content-end">
                        <p class="copyright">Copyright 2020 Achieve3000 | All rights reserved</p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
         <!--- footer section end--->
      </div>
      <!-- JS -->
      <script type="text/javascript" src="{{ asset('public/js/jquery-3.5.1.min.js') }}" ></script>
      <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}" ></script>
      <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
      <script src="https://cdn.tiny.cloud/1/ysq7q2ykseku2zt06dkphxtjppgteshxvtd30zg7kwkhoxfu/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
      <script src="{{ asset('public/vendor/jquery-validation/dist/jquery.validate.js') }}"></script>      
      <script src="{{ asset('public/vendor/jquery-validation/dist/additional-methods.js') }}"></script>  
      <script type="text/javascript" src="{{asset('public/backend/js/inputmask.min.js')}}"></script>
      <script src="{{asset('public/backend/js/jquery-input-mask-phone-number.js')}}"></script>      
      <script src="{{asset('public/vendor/jquery-validation/src/additional/phoneUS.js')}}"></script>
      <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
      <script src="{{ asset('public/backend/js/admin.js?v='.time())}}" type="text/javascript"></script>
      <script src="{{ asset('public/backend/js/toastr.min.js') }}"></script>
      <script type="text/javascript" src="{{ asset('public/js/custom.js') }}" ></script>
      <script src='https://kit.fontawesome.com/a076d05399.js'></script>
   </body>
</html>