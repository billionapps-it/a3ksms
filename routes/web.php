<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// clear chache route
Route::get('clear-cache', function() {
   $exitCode    = Artisan::call('cache:clear');
   $config      = Artisan::call('config:cache');
   $view        = Artisan::call('view:clear');
   return "Cache is cleared";
});


Auth::routes();

/********************
* ADMIN ROLE ROUTES *
*********************/
Route::group(['middleware' => ['admin']], function () {

    //Dashboard Routs Start
	Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard');
	Route::match(['get','post'],'Admin\rolePermissions', 'RolePermissionController@index');

	//Users Routs 
	Route::match(['get','post'],'users', 'Admin\UserController@index');
	Route::match(['get','post'],'user/store', 'Admin\UserController@store');
	Route::match(['get','post'],'user/edit/{id}', 'Admin\UserController@edit');
	Route::post('users/deleteuserall', 'Admin\UserController@deleteuserall');

	//Manage Sms Tempaltes Routs 
	Route::match(['get','post'],'templates', 'Admin\SmsTemplateController@index');
	Route::match(['get','post'],'template/createtemplate', 'Admin\SmsTemplateController@createtemplate');
	Route::match(['get','post'],'template/edittemplate/{id}', 'Admin\SmsTemplateController@edittemplate');
	Route::match(['get','post'],'template/deletetemplateall', 'Admin\SmsTemplateController@deletetemplateall');
	Route::match(['get','post'],'template/variablecreate/store', 'Admin\SmsTemplateController@variablecreate_store');
	Route::match(['get','post'],'template/other/message_type', 'Admin\SmsTemplateController@message_type_store');
	Route::match(['get','post'],'getTemplatePreview', 'Admin\SmsTemplateController@getTemplatePreview');
	Route::get('template/get_possible_values', 'Admin\SmsTemplateController@get_possible_values');

	//send sms
	Route::match(['get','post'],'sms/sendmessage', 'Admin\SendSMSController@store');
	Route::post('template/ajaxUploadCSV', 'Admin\SendSMSController@ajaxUploadCSV');	
	Route::post('template/get_template_data', 'Admin\SendSMSController@get_template_data');
	Route::post('sendsms/check_variables_mapping', 'Admin\SendSMSController@check_variables_mapping');
	Route::get('sendsms/download_error_logs', 'Admin\SendSMSController@download_error_logs');
	
	//Settings
	Route::get('settings/appsetting', 'Admin\SettingsController@app_setting');
	Route::match(['get','post'],'settings/createapp', 'Admin\SettingsController@createapp');
	Route::match(['get','post'],'settings/editapp/{id}', 'Admin\SettingsController@editapp');
	Route::get('settings/deleteapp/{id}', 'Admin\SettingsController@deleteApp');

	//Settings
	Route::get('settings/messagetypesetting', 'Admin\SettingsController@message_type_setting');
	Route::match(['get','post'],'settings/createmessagetype', 'Admin\SettingsController@create_message_type');
	Route::match(['get','post'],'settings/editmessagetype/{id}', 'Admin\SettingsController@edit_message_type');
	Route::get('settings/deletemessagetype/{id}', 'Admin\SettingsController@delete_message_type');

	//Reports
	Route::match(['get','post'],'reports/delivery_reports', 'Admin\ReportsController@delivery_reports');
	Route::post('reports/export', 'Admin\ReportsController@export');
	Route::match(['get','post'],'reports/scheduled_reports', 'Admin\ReportsController@scheduled_reports');
	Route::get('reports/cancel/{id}', 'Admin\ReportsController@cancel_report');
	Route::get('reports/delete/{id}', 'Admin\ReportsController@delete_report');
	Route::post('reports/re_schedule', 'Admin\ReportsController@re_schedule');
});
Route::match(['post'],'login/action', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');	
Route::get('/', function () { return redirect('login'); });

//front
Route::match(['get','post'],'resetpassword/{token}', 'Front\FrontController@resetpassword');